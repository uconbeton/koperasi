<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\UnitKerjaController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PinjamanController;
use App\Http\Controllers\KeuanganController;
use App\Http\Controllers\SimpananController;
use App\Http\Controllers\TabelPinjamanController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\Auth\LogoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return 'Config cache cleared';
});
Route::group(['middleware' => 'auth'], function() {
    Route::get('/logout-perform', [LogoutController::class, 'perform'])->name('logout.perform');

    Route::group(['prefix' => 'master'], function () {
        Route::group(['prefix' => '/jabatan'], function () {
            Route::get('/', [JabatanController::class, 'index']);
            Route::get('/add', [JabatanController::class, 'add']);
            Route::get('/get_data', [JabatanController::class, 'get_data']);
            Route::post('/', [JabatanController::class, 'store']);
            Route::get('/delete_data', [JabatanController::class, 'delete_data']);
        });
        Route::group(['prefix' => '/unit_kerja'], function () {
            Route::get('/', [UnitKerjaController::class, 'index']);
            Route::get('/add', [UnitKerjaController::class, 'add']);
            Route::get('/get_data', [UnitKerjaController::class, 'get_data']);
            Route::post('/', [UnitKerjaController::class, 'store']);
            Route::get('/delete_data', [UnitKerjaController::class, 'delete_data']);
        });
        Route::group(['prefix' => '/tabel_pinjaman'], function () {
            Route::get('/', [TabelPinjamanController::class, 'index']);
            Route::get('/add', [TabelPinjamanController::class, 'add']);
            Route::get('/get_data', [TabelPinjamanController::class, 'get_data']);
            Route::post('/', [TabelPinjamanController::class, 'store']);
            Route::get('/delete_data', [TabelPinjamanController::class, 'delete_data']);
        });
        Route::group(['prefix' => '/kategori'], function () {
            Route::get('/', [KategoriController::class, 'index']);
            Route::get('/add', [KategoriController::class, 'add']);
            Route::get('/get_data', [KategoriController::class, 'get_data']);
            Route::post('/', [KategoriController::class, 'store']);
            Route::get('/delete_data', [KategoriController::class, 'delete_data']);
        });
    });
    Route::group(['prefix' => '/anggota'], function () {
        Route::get('/', [AnggotaController::class, 'index']);
        Route::get('/add', [AnggotaController::class, 'add']);
        Route::get('/detail', [AnggotaController::class, 'detail_data']);
        Route::get('/get_data', [AnggotaController::class, 'get_data']);
        Route::post('/', [AnggotaController::class, 'store']);
        Route::get('/delete_data', [AnggotaController::class, 'delete_data']);
    });
    Route::group(['prefix' => '/keuangan'], function () {
        Route::get('/', [KeuanganController::class, 'index']);
        Route::get('/add', [KeuanganController::class, 'add']);
        Route::get('/get_data', [KeuanganController::class, 'get_data']);
        Route::post('/', [KeuanganController::class, 'store']);
        Route::get('/delete_data', [KeuanganController::class, 'delete_data']);
    });
    Route::group(['prefix' => '/simpanan'], function () {
        Route::get('/', [SimpananController::class, 'index']);
        Route::get('/add', [SimpananController::class, 'add']);
        Route::get('/detail', [SimpananController::class, 'detail_data']);
        Route::get('/get_data', [SimpananController::class, 'get_data']);
        Route::get('/get_data_detail', [SimpananController::class, 'get_data_detail']);
        Route::post('/', [SimpananController::class, 'store']);
        Route::get('/delete_data', [SimpananController::class, 'delete_data']);
    });
    Route::group(['prefix' => '/pinjaman'], function () {
        Route::get('/', [PinjamanController::class, 'index']);
        Route::get('/aktif', [PinjamanController::class, 'index']);
        Route::get('/lunas', [PinjamanController::class, 'index_lunas']);
        Route::get('/add', [PinjamanController::class, 'add']);
        Route::get('/tentukan_hasil', [PinjamanController::class, 'tentukan_hasil']);
        Route::get('/get_data', [PinjamanController::class, 'get_data']);
        Route::get('/get_data_lunas', [PinjamanController::class, 'get_data_lunas']);
        Route::get('/get_data_detail', [PinjamanController::class, 'get_data_detail']);
        Route::post('/', [PinjamanController::class, 'store']);
        Route::get('/delete_data', [PinjamanController::class, 'delete_data']);
        Route::get('/proses_data', [PinjamanController::class, 'proses_data']);
        Route::get('/modal_bayar', [PinjamanController::class, 'modal_bayar']);
        Route::get('/detail_data', [PinjamanController::class, 'detail_data']);
    });


    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/settingapp', [App\Http\Controllers\SettingController::class, 'index']);
    Route::post('/settingapp/store_user', [App\Http\Controllers\SettingController::class, 'store_user']);
});
Auth::routes();