<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\Simpanan;
use App\Models\ViewAnggota;
use App\Models\Anggota;
use App\Models\MTipe;

class SimpananController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        $tipe_id=decoder($request->tipe);
        if($request->tahun==""){
            $tahun=date('Y');
        }else{
            $tahun=$request->tahun;
        }
        $data=MTipe::where('id',$tipe_id)->first();
        return view('simpanan.index',compact('data','tipe_id','tahun'));
    }
    public function detail_data(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $tipe_id=decoder($request->tipe_id);
        $data=Anggota::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('simpanan.detail_data',compact('data','id','disabled','tipe_id'));
    }
    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('simpanan.add',compact('data','id','disabled'));
    }

    public function get_data(request $request)
    {
        $data = Anggota::where('active',1)->orderBy('id', 'DESC')->get();
        $tipe_id=decoder($request->tipe_id);
        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-clipboard-list"></i> Detail</span>
                    </div>
                    ';
                return $btn;
            })
            ->addColumn('selectdata', function ($row) {
                $btn = '
                    <input type="checkbox" name="no_anggota[]" value="'.$row->no_anggota.'">
                    ';
                return $btn;
            })
            ->addColumn('pilih', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Pilih" onclick="pilih_data(`' . $row['no_anggota'] . '`,`' . $row['no_register'] . '`,`' . $row['nama'] . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i> Pilih</span>
                     </div>
                    ';
                return $btn;
            })
            ->addColumn('updatedat', function ($row) use ($tipe_id) {
                $btn = updatedat_simpanan($tipe_id,$row->no_anggota);
                return $btn;
            })
            ->addColumn('saldo', function ($row) use ($tipe_id) {
                $btn = 'Rp. '.uang(nilai_simpanan($tipe_id,$row->no_anggota));
                return $btn;
            })
            ->addColumn('jumlah_simpanan', function ($row) use ($tipe_id) {
                $btn = jumlah_simpanan($tipe_id,$row->no_anggota).'X';
                return $btn;
            })
            ->rawColumns([
                'action','pilih','selectdata'
            ])
            ->make(true);
    }
    public function get_data_detail(request $request)
    {
        $data = Simpanan::where('no_anggota',$request->no_anggota)->where('tipe_id',$request->tipe_id)->orderBy('id', 'DESC')->get();
        $tipe_id=decoder($request->tipe_id);
        return  DataTables::of($data)->addIndexColumn()
            
            ->addColumn('nilai', function ($row)  {
                $btn = 'Rp. '.uang($row->nilai);
                return $btn;
            })
            ->addColumn('bulan', function ($row)  {
                $btn = bulan($row->bulan);
                return $btn;
            })
           
            ->rawColumns([
                'nilai'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        error_reporting(0);
        $rules = [];
        $messages = [];
        $count=count((array) $request->no_anggota);
        
        if($count==0){
            // $rules['xxxxxx'] = 'required';
            // $messages['xxxxxx.required'] = 'Pilih Anggota Terlebih Dahulu';
            echo 'Pilih Anggota Terlebih Dahulu';
        }else{
            
            $rules['bulan'] = 'required';
            $messages['bulan.required'] = 'Bulan tidak boleh kosong';

            $rules['tahun'] = 'required';
            $messages['tahun.required'] = 'Bulan tidak boleh kosong';

            $rules['nilai'] = 'required';
            $messages['nilai.required'] = 'Nilai tidak boleh kosong';

            $rules['tgl_simpanan'] = 'required';
            $messages['tgl_simpanan.required'] = 'Tanggal pembayaran tidak boleh kosong';

            $rules['nilai']= 'required|min:0|not_in:0';
            $messages['nilai.min']= 'Nilai tidak boleh kosong';
            $messages['nilai.not_in']= 'Nilai tidak boleh kosong';
            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                


                for($x=0;$x<$count;$x++){
                    if($request->tipe_id==3){
                        $cekdata=Simpanan::where('no_anggota',$request->no_anggota[$x])->where('bulan',$request->bulan)->where('tahun',$request->tahun)->count();
                        if($cekdata>0){

                        }else{
                            $no_transaksi_simpanan=no_transaksi_simpanan($request->tipe_id);
                            $save=Simpanan::UpdateOrcreate([
                                'no_anggota'=>$request->no_anggota[$x],
                                'bulan'=>$request->bulan,
                                'tahun'=>$request->tahun,
                            ],[
                                'status_keuangan_id'=>$request->status_keuangan_id,
                                'tgl_simpanan'=>$request->tgl_simpanan,
                                'no_transaksi_simpanan'=>$no_transaksi_simpanan,
                                'tipe_id'=>$request->tipe_id,
                                
                                'nilai'=>ubah_uang($request->nilai),
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'active'=>1,
                            ]);

                            $no_transaksi=$no_transaksi_simpanan;
                            $keterangan_keuangan='Simpanan Wajib Periode '.bulan($request->bulan).' '.$request->tahun.' Dari '.anggota($request->no_anggota[$x])['nama'];
                            
                            
                            $status_keuangan_id=$request->status_keuangan_id;
                            $tipe_id=$request->tipe_id;
                            $no_anggota=$request->no_anggota[$x];
                            $tanggal=$request->tgl_simpanan;
                            $nilai=ubah_uang($request->nilai);
                            $nilai_provit=0;
                            create_keuangan($no_transaksi,$keterangan_keuangan,$status_keuangan_id,$tipe_id,$nilai,$nilai_provit,$no_anggota,$tanggal);
                        }
                    }else{
                        $no_transaksi_simpanan=no_transaksi_simpanan($request->tipe_id);
                            $save=Simpanan::Create([
                                'no_anggota'=>$request->no_anggota[$x],
                                'bulan'=>$request->bulan,
                                'tahun'=>$request->tahun,
                           
                                'status_keuangan_id'=>$request->status_keuangan_id,
                                'tgl_simpanan'=>$request->tgl_simpanan,
                                'no_transaksi_simpanan'=>$no_transaksi_simpanan,
                                'tipe_id'=>$request->tipe_id,
                                
                                'nilai'=>ubah_uang($request->nilai),
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'),
                                'active'=>1,
                            ]);

                            $no_transaksi=$no_transaksi_simpanan;
                            if($request->tipe_id==4){
                                $keterangan_keuangan='Simpanan Pokok Dari '.anggota($request->no_anggota[$x])['nama'];
                            }
                            if($request->tipe_id==5){
                                $keterangan_keuangan='Simpanan Sukarela Dari '.anggota($request->no_anggota[$x])['nama'];
                            }
                            
                            $status_keuangan_id=$request->status_keuangan_id;
                            $tipe_id=$request->tipe_id;
                            $no_anggota=$request->no_anggota[$x];
                            $tanggal=$request->tgl_simpanan;
                            $nilai=ubah_uang($request->nilai);
                            $nilai_provit=0;
                            create_keuangan($no_transaksi,$keterangan_keuangan,$status_keuangan_id,$tipe_id,$nilai,$nilai_provit,$no_anggota,$tanggal);
                    }
                    
                }
                    
    
                echo '@ok';
                
                
            }
        }
        
    }
}
