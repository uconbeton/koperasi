<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;
use Illuminate\Support\Facades\Hash;
use App\Models\Anggota;
use App\Models\User;
use App\Models\ViewAnggota;

class SettingController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('setting.index');
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('anggota.add',compact('data','id','disabled'));
    }

    public function get_data(request $request)
    {
        $data = ViewAnggota::where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                return $btn;
            })
            ->addColumn('pilih', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Pilih" onclick="pilih_data(`' . $row['no_anggota'] . '`,`' . $row['no_register'] . '`,`' . $row['nama'] . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i> Pilih</span>
                     </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action','pilih'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->update(['active'=>0]);
    }

    public function store_user(request $request){
        $rules = [];
        $messages = [];

            $rules['value'] = 'required';
            $messages['value.required'] = 'Kolom tidak boleh kosong';

            
            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                    if($request->tipe=='password'){
                        $save=User::where('id',Auth::user()->id)->update([
                            'password'=>Hash::make($request->value),
                        ]);
                    }else{
                        $save=User::where('id',Auth::user()->id)->update([
                            $_POST['tipe']=>$request->value,
                        ]);
                    }
                    
    
                    echo '@ok';
                
            }
        
    }
}
