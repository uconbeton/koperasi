<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\MKategori;

class KategoriController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('master.kategori.index');
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=MKategori::where('id',$id)->first();
        return view('master.kategori.add',compact('data','id'));
    }

    public function get_data(request $request)
    {
        $data = MKategori::where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=MKategori::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

        
            $rules['kategori'] = 'required';
            $messages['kategori.required'] = 'Kategori tidak boleh kosong';

            $rules['persen'] = 'required|numeric';
            $messages['persen.required'] = 'Persen tidak boleh kosong';

            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                if($request->id==0){
                    $save=MKategori::create([
                        'kategori'=>$request->kategori,
                        'persen'=>$request->persen,
                        'active'=>1,
                    ]);
    
                    echo '@ok';
                }else{
                    $save=MKategori::where('id',$request->id)->update([
                        'kategori'=>$request->kategori,
                        'persen'=>$request->persen,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
