<?php

namespace App\Http\Controllers\Api;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use App\Models\ViewDetailPinjaman;
use App\Models\DetailPinjaman;
class ApiController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function json(request $request){
        $bulan="";
        $nilaix="";
        $nilaiy="";
        for($x=1;$x<=100;$x++){
            $bul='SSS '.$x;
            if($x==100){
                $bulan.='"'.$bul.'"';
                $nilaiy.=($x+15);
                $nilaix.=$x;
            }else{
                $bulan.='"'.$bul.'",';
                $nilaiy.=($x+15).',';
                $nilaix.=$x.',';
            }
            
        }
        $json='{
            "chart": {
              "type": "area",
              "height": 350
            },
            "stroke": { "curve": "smooth", "width": 3 },
            "dataLabels": { "enabled": false },
            "series": [
              {
                "name": "Series 1",
                "data": ['.$nilaix.']
              },
              {
                "name": "Series 2",
                "data": ['.$nilaiy.']
              }
            ],
            "xaxis": {
              "categories": ['.$bulan.'],
              "tickAmount": 10
            },
            "tooltip": {
              "enabled": true
            }
          }';
        return $json;
    }
    public function jsons(request $request){
        $bulan="";
        $nilai="";
        for($x=1;$x<=12;$x++){
            $bul=bulan(ubah_bulan($x));
            if($x==12){
                $bulan.="'$bul'";
                $nilai.=$x;
            }else{
                $bulan.="'$bul',";
                $nilai.=$x.',';
            }
            
        }
        $json='{
            "chart": {
              "type": "area",
              "height": 350
            },
            stroke: { curve: "smooth", width: 3 },
            colors: [app.color.pink, app.color.dark],
            "series": [
              {
                "name": "Series 1",
                "data": ['.$nilai.']
              },
              {
                "name": "Series 2",
                "data": ['.$nilai.']
              }
            ],
            "xaxis": {
              "categories": ['.$bulan.']
            },
            "tooltip": {
              "enabled": true
            }
          }';
        return $json;
    }
    public function get_lunas(request $request){
        $data=ViewDetailPinjaman::where('tgl_tagihan','<',date('Y-m-d'))->get();
        foreach($data as $o){
            $save=DetailPinjaman::where('id',$o->id)->update(['tgl_bayar'=>$o->tgl_tagihan]);
            $no_transaksi=$o->no_transaksi_angsuran;
            $keterangan_keuangan='Pembayaran pinjaman '.$o->no_pinjaman.' Ke - '.$o->angsuran.' Rp.'.uang($o->nilai).' dari '.$o->nama;
            $status_keuangan_id=$o->status_keuangan_id;
            $tipe_id=$o->tipe_id;
            $no_anggota=$o->no_anggota;
            $tanggal=$o->tgl_tagihan;
            $nilai=$o->nilai;
            $nilai_provit=$o->bunga;
            create_keuangan($no_transaksi,$keterangan_keuangan,$status_keuangan_id,$tipe_id,$nilai,$nilai_provit,$no_anggota,$tanggal);
        }
    }
    public function index(request $request){
        $model_1=$request->value_model_1;
        $model_2=$request->value_model_2;
        $model_3=$request->value_model_3;
        $model_4=$request->value_model_4;
        $success=[];
        if($model_1>0){
            $success['model_1']='<i class="bi bi-rss-fill fs-35px h-150px d-flex align-items-center justify-content-center bg-green mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-danger me-5px" onclick="run_model_1(0)"><i class="fa fa-circle-check"></i> Stop</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }else{
            $success['model_1']='<i class="bi bi-rss fs-35px h-150px d-flex align-items-center justify-content-center bg-gray mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" onclick="run_model_1(1)" class="btn btn-xs d-block btn-indigo me-5px"><i class="fa fa-circle-check"></i> Runing</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }
        if($model_2>0){
            $success['model_2']='<i class="bi bi-rss-fill fs-35px h-150px d-flex align-items-center justify-content-center bg-green mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-danger me-5px" onclick="run_model_2(0)"><i class="fa fa-circle-check"></i> Stop</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }else{
            $success['model_2']='<i class="bi bi-rss fs-35px h-150px d-flex align-items-center justify-content-center bg-gray mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" onclick="run_model_2(1)" class="btn btn-xs d-block btn-indigo me-5px"><i class="fa fa-circle-check"></i> Runing</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }
        if($model_3>0){
            $success['model_3']='<i class="bi bi-rss-fill fs-35px h-150px d-flex align-items-center justify-content-center bg-green mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-danger me-5px" onclick="run_model_3(0)"><i class="fa fa-circle-check"></i> Stop</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }else{
            $success['model_3']='<i class="bi bi-rss fs-35px h-150px d-flex align-items-center justify-content-center bg-gray mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" onclick="run_model_3(1)" class="btn btn-xs d-block btn-indigo me-5px"><i class="fa fa-circle-check"></i> Runing</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }
        if($model_4>0){
            $success['model_4']='<i class="bi bi-rss-fill fs-35px h-150px d-flex align-items-center justify-content-center bg-green mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-danger me-5px" onclick="run_model_4(0)"><i class="fa fa-circle-check"></i> Stop</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }else{
            $success['model_4']='<i class="bi bi-rss fs-35px h-150px d-flex align-items-center justify-content-center bg-gray mb-2" id="iii"></i> 
                                    activity
                                    <div class="row" style="margin: 4% 1% 1% 1%;;background: #cccddc;">
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" onclick="run_model_4(1)" class="btn btn-xs d-block btn-indigo me-5px"><i class="fa fa-circle-check"></i> Runing</a>
                                        </div>
                                        <div class="col-md-6" style="padding:1%">
                                            <a href="javascript:;" class="btn btn-xs d-block btn-warning me-5px"><i class="fa fa-clone"></i> Log</a>
                                        </div>
                                        
                                    </div>';
        }
        
        return response()->json($success, 200);
    }
}
