<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\Pinjaman;
use App\Models\ViewPinjaman;
use App\Models\ViewDetailPinjaman;
use App\Models\MTabelpinjaman;
use App\Models\DetailPinjaman;

class PinjamanController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('pinjaman.index');
    }
    public function index_lunas(request $request){
        return view('pinjaman.index_lunas');
    }
    public function tentukan_hasil(request $request){
        $tabel_pinjaman=MTabelpinjaman::where('id',$request->tabel_pinjaman_id)->first();
        $angsur=($tabel_pinjaman->nilai/$request->lama_angsuran);
        $jumlah_bunga=($tabel_pinjaman->nilai*bunga())/100;
        $angsuran_perbulan=ceil($angsur+$jumlah_bunga);
        echo'
            <u><h4>RINCIAN PINJAMAN</h4></u>
            <table width="100%">
                
                <tr>
                    <td width="7%"></td>
                    <td width="27%"><b>Jumlah Pinjaman</b></td>
                    <td width="4%"><b>:</b></td>
                    <td ><b>Rp.'.uang($tabel_pinjaman->nilai).'</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td><b>Jumlah Angsuran</b></td>
                    <td><b>:</b></td>
                    <td ><b>'.$request->lama_angsuran.'X</b></td>
                </tr>
                <tr>
                    <td ></td>
                    <td ><b>Bunga %</b></td>
                    <td ><b>:</b></td>
                    <td ><b>'.bunga().' %</b></td>
                </tr>
                <tr>
                    <td ></td>
                    <td ><b>Bunga Pinjaman /bulan</b></td>
                    <td ><b>:</b></td>
                    <td ><b>Rp'.uang($jumlah_bunga).' </b></td>
                </tr>
                <tr>
                    <td ></td>
                    <td ><b>Angsuran /bulan</b></td>
                    <td ><b>:</b></td>
                    <td ><b>Rp'.uang($angsuran_perbulan).' </b></td>
                </tr>
                
            </table>
            <input type="hidden" name="nilai" value="'.$tabel_pinjaman->nilai.'">
            <input type="hidden" name="angsuran_perbulan" value="'.$angsuran_perbulan.'">
            <input type="hidden" name="bunga_perbulan" value="'.$jumlah_bunga.'">
            <input type="hidden" name="bunga" value="'.bunga().'">
        ';
        // for($x=1;$x<=$request->lama_angsuran;$x++){
        //     $date=date('Y-m-d');
        //     echo nextbulan($date,$x).'<br>';
        // }
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=ViewPinjaman::where('status_pinjaman',1)->where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('pinjaman.add',compact('data','id','disabled'));
    }
    public function modal_bayar(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=ViewDetailPinjaman::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('pinjaman.modal_bayar',compact('data','id','disabled'));
    }
    public function detail_data(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=ViewPinjaman::where('status_pinjaman','>',1)->where('id',$id)->first();
        
        return view('pinjaman.detail_data',compact('data','id'));
    }

    public function get_data(request $request)
    {
        $data = ViewPinjaman::where('active',1)->where('status_pinjaman','!=',3)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                if($row->status_pinjaman==1){
                    $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Hapus" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                        <span title="Publis Pinjaman" onclick="proses_data(`' . encoder($row['id']) . '`)" class="btn btn-secondary btn-xs"><i class="fas fa-share-from-square"></i></span>
                    </div>
                    ';
                }else{
                    $btn = '
                    <div class="btn-group">
                        <span title="Ubah" class="btn btn-light btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Hapus" class="btn btn-light btn-xs"><i class="fas fa-trash-can"></i></span>
                        <span title="Lihat Detail" onclick="detail_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-search"></i></span>
                    </div>
                    ';
                }
                return $btn;
            })
            ->addColumn('nilai', function ($row) {
                return uang($row->nilai);
            })
            ->addColumn('lama_angsuran', function ($row) {
                return $row->lama_angsuran.'X';
            })
            ->addColumn('angsuran_perbulan', function ($row) {
                return uang($row->angsuran_perbulan);
            })
            ->addColumn('bunga_perbulan', function ($row) {
                return uang($row->bunga_perbulan);
            })
            ->addColumn('nilai', function ($row) {
                return uang($row->nilai);
            })
            ->addColumn('status', function ($row) {
                if($row->status_pinjaman==1){
                    return '<span class="badge bg-warning">'.$row->sts_pinjaman.'</span>';
                }
                if($row->status_pinjaman==2){
                    return '<span class="badge bg-success">'.$row->sts_pinjaman.'</span>';
                }
                if($row->status_pinjaman==3){
                    return '<span class="badge bg-primary">'.$row->sts_pinjaman.'</span>';
                }
            })
            ->rawColumns([
                'action','status'
            ])
            ->make(true);
    }
    public function get_data_lunas(request $request)
    {
        $data = ViewPinjaman::where('active',1)->where('status_pinjaman',3)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
               
                    $btn = '
                    <div class="btn-group">
                        <span title="Lihat Detail" onclick="detail_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-search"></i></span>
                    </div>
                    ';
                
                return $btn;
            })
            ->addColumn('nilai', function ($row) {
                return uang($row->nilai);
            })
            ->addColumn('lama_angsuran', function ($row) {
                return $row->lama_angsuran.'X';
            })
            ->addColumn('angsuran_perbulan', function ($row) {
                return uang($row->angsuran_perbulan);
            })
            ->addColumn('bunga_perbulan', function ($row) {
                return uang($row->bunga_perbulan);
            })
            ->addColumn('nilai', function ($row) {
                return uang($row->nilai);
            })
            ->addColumn('status', function ($row) {
                if($row->status_pinjaman==1){
                    return '<span class="badge bg-warning">'.$row->sts_pinjaman.'</span>';
                }
                if($row->status_pinjaman==2){
                    return '<span class="badge bg-success">'.$row->sts_pinjaman.'</span>';
                }
                if($row->status_pinjaman==3){
                    return '<span class="badge bg-primary">'.$row->sts_pinjaman.'</span>';
                }
            })
            ->rawColumns([
                'action','status'
            ])
            ->make(true);
    }
    public function get_data_detail(request $request)
    {
        $data = DetailPinjaman::where('pinjaman_id',$request->pinjaman_id)->orderBy('id', 'Asc')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                if($row->status_angsuran==1){
                    $btn = '
                    <div class="btn-group">
                        <span title="Bayar Pinjaman" onclick="tampil_bayar(`' . encoder($row['id']) . '`)" class="btn btn-secondary btn-xs"><i class="fas fa-share-from-square"></i> Bayar</span>
                    </div>
                    ';
                }else{
                    $btn = '
                    <div class="btn-group">
                        <span title="Ubah" class="btn btn-light btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Hapus" class="btn btn-light btn-xs"><i class="fas fa-trash-can"></i></span>
                        <span title="Lihat Detail" onclick="detail_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-search"></i></span>
                    </div>
                    ';
                }
                return $btn;
            })
            ->addColumn('nilai', function ($row) {
                return 'Rp.'.uang($row->nilai);
            })
            ->addColumn('bunga', function ($row) {
                return 'Rp.'.uang($row->bunga);
            })
            ->addColumn('angsuran', function ($row) {
                return $row->angsuran.'X';
            })
            ->addColumn('shu', function ($row) {
                return 'Rp.'.uang($row->shu);
            })
            
            ->rawColumns([
                'action'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=Pinjaman::where('id',$id)->update(['active'=>0]);
    }
    public function proses_data(request $request){
        $id=decoder($request->id);
        $data=ViewPinjaman::where('id',$id)->first();
        
        $save=Pinjaman::where('id',$id)->update(['status_pinjaman'=>2]);
        $date=$data->tgl_pinjam;
        for($x=1;$x<=$data->lama_angsuran;$x++){
            $shu=((persen_kategori(7)*$data->bunga_perbulan)/100);
            $savedetaik=DetailPinjaman::UpdateOrCreate([
                'no_pinjaman'=>$data->no_pinjaman,
                'pinjaman_id'=>$data->id,
                'no_anggota'=>$data->no_anggota,
                'angsuran'=>$x,
            ],[
                'status_angsuran'=>1, 
                'no_transaksi_angsuran'=>no_transaksi_angsuran(), 
                'tipe_id'=>1,
                'status_keuangan_id'=>1,
                'nilai'=>$data->angsuran_perbulan,
                'bunga'=>$data->bunga_perbulan,
                'shu'=>$shu,
                'sisa_bagi'=>$data->bunga_perbulan-$shu,
                'bunga'=>$data->bunga_perbulan,
                'tgl_tagihan'=>nextbulan($date,$x),
                'bulan'=>hanyabulan(nextbulan($date,$x)),
                'tahun'=>hanyatahun(nextbulan($date,$x)),
            ]);
            
        }
        // Variable Keuangan================================================================
        $no_transaksi=$data->no_pinjaman;
        $keterangan_keuangan='Pemberian pinjaman Rp.'.uang($data->nilai).' kepada '.$data->nama;
        $status_keuangan_id=$data->status_keuangan_id;
        $tipe_id=$data->tipe_id;
        $no_anggota=$data->no_anggota;
        $tanggal=date('Y-m-d');
        $nilai=$data->nilai;
        $nilai_provit=0;
        return create_keuangan($no_transaksi,$keterangan_keuangan,$status_keuangan_id,$tipe_id,$nilai,$nilai_provit,$no_anggota,$tanggal);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

            if($request->id==0){
                $rules['no_anggota'] = 'required';
                $messages['no_anggota.required'] = 'Anggota tidak boleh kosong';
            }
            $rules['tabel_pinjaman_id'] = 'required';
            $messages['tabel_pinjaman_id.required'] = 'Jumlah Pinjaman tidak boleh kosong';

            $rules['lama_angsuran'] = 'required';
            $messages['lama_angsuran.required'] = 'Jumlah Angsuran tidak boleh kosong';

            $rules['tgl_pinjam'] = 'required';
            $messages['tgl_pinjam.required'] = 'Tanggal Pinjaman tidak boleh kosong';

            

            $rules['keterangan'] = 'required';
            $messages['keterangan.required'] = 'Alasan Pinjaman tidak boleh kosong';

            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                if($request->id==0){
                    $save=Pinjaman::create([
                        'no_pinjaman'=>no_pinjaman(),
                        'no_anggota'=>$request->no_anggota,
                        'tabel_pinjaman_id'=>$request->tabel_pinjaman_id,
                        'nilai'=>$request->nilai,
                        'keterangan'=>$request->keterangan,
                        'lama_angsuran'=>$request->lama_angsuran,
                        'tgl_pinjam'=>$request->tgl_pinjam,
                        'tipe_id'=>1,
                        'status_keuangan_id'=>2,
                        'angsuran_perbulan'=>$request->angsuran_perbulan,
                        'bunga'=>$request->bunga,
                        'bunga_perbulan'=>$request->bunga_perbulan,
                        'status_pinjaman'=>1,
                        'active'=>1,
                    ]);
    
                    echo '@ok';
                }else{
                    $save=Pinjaman::where('id',$request->id)->update([
                        'no_anggota'=>$request->no_anggota,
                        'tabel_pinjaman_id'=>$request->tabel_pinjaman_id,
                        'nilai'=>$request->nilai,
                        'keterangan'=>$request->keterangan,
                        'lama_angsuran'=>$request->lama_angsuran,
                        'tgl_pinjam'=>$request->tgl_pinjam,
                        'angsuran_perbulan'=>$request->angsuran_perbulan,
                        'bunga'=>$request->bunga,
                        'bunga_perbulan'=>$request->bunga_perbulan,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
