<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\MTabelpinjaman;

class TabelPinjamanController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('master.tabel_pinjaman.index');
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=MTabelpinjaman::where('id',$id)->first();
        return view('master.tabel_pinjaman.add',compact('data','id'));
    }

    public function get_data(request $request)
    {
        $data = MTabelpinjaman::where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=MTabelpinjaman::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

        
            $rules['nilai'] = 'required|numeric';
            $messages['nilai.required'] = 'Nilai tidak boleh kosong';

            $rules['keterangan_pinjaman'] = 'required';
            $messages['keterangan_pinjaman.required'] = 'Keterangan tidak boleh kosong';

            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                if($request->id==0){
                    $save=MTabelpinjaman::create([
                        'nilai'=>$request->nilai,
                        'keterangan_pinjaman'=>$request->keterangan_pinjaman,
                        'active'=>1,
                    ]);
    
                    echo '@ok';
                }else{
                    $save=MTabelpinjaman::where('id',$request->id)->update([
                        'nilai'=>$request->nilai,
                        'keterangan_pinjaman'=>$request->keterangan_pinjaman,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
