<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\Keuangan;
use App\Models\ViewKeuangan;

class KeuanganController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        if($request->tahun==""){
            $tahun=date('Y');
        }else{
            $tahun=$request->tahun;
        }
        return view('keuangan.index',compact('tahun'));
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=Keuangan::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('keuangan.modal',compact('data','id','disabled'));
    }

    public function get_data(request $request)
    {
        $data = ViewKeuangan::where('tahun',$request->tahun)->where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                if($row->tipe_id==1){
                    $btn = '
                    <div class="btn-group">
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                }else{
                    $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                }
                return $btn;
            })
            ->addColumn('status_keuangannya', function ($row) {
                if($row->status_keuangan_id==1){
                    $btn = '
                    <span class="badge bg-indigo rounded-0">Masuk</span>
                    ';
                }elseif($row->status_keuangan_id==2){
                    $btn = '
                        <span class="badge bg-lime rounded-0">Keluar</span>
                    ';
                }else{
                    $btn = '
                        <span class="badge bg-warning rounded-0">Hutang</span>
                    ';
                }
                return $btn;
            })
            ->addColumn('nilai', function ($row) {
                return 'Rp.'.uang($row->nilai);
            })
            ->addColumn('nilai_provit', function ($row) {
                return 'Rp.'.uang($row->nilai_provit);
            })
            ->addColumn('bulan', function ($row) {
                return bulan($row->bulan);
            })
            ->addColumn('pilih', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Pilih" onclick="pilih_data(`' . $row['no_anggota'] . '`,`' . $row['no_register'] . '`,`' . $row['nama'] . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i> Pilih</span>
                     </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action','pilih','status_keuangannya'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=Keuangan::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

            
            $rules['status_keuangan_id'] = 'required';
            $messages['status_keuangan_id.required'] = 'Status Keuangan tidak boleh kosong';

            $rules['keterangan_keuangan'] = 'required';
            $messages['keterangan_keuangan.required'] = 'Keterangan tidak boleh kosong';

            $rules['nilai']= 'required|min:0|not_in:0';
            $messages['nilai.min']= 'Nilai tidak boleh kosong';
            $messages['nilai.not_in']= 'Nilai tidak boleh kosong';

            $rules['tanggal'] = 'required';
            $messages['tanggal.required'] = 'Tanggal  tidak boleh kosong';

           
            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                $nilai=ubah_uang($request->nilai);
                $exp=explode('-',$request->tanggal);
                    $bulan=$exp[1];
                    $tahun=$exp[0];
                    $tgl=$exp[2];
                    $no_transaksi='LA'.date('ymdhis');
                if($request->id==0){
                    
                    $save =Keuangan::UpdateOrcreate([
                        'tipe_id'=>6,
                        'no_transaksi'=>$no_transaksi,
                    ],[
                        'status_keuangan_id'=>$request->status_keuangan_id,
                        'nilai'=>$nilai,
                        'nilai_provit'=>0,
                        'no_anggota'=>0,
                        'tahun'=>$tahun,
                        'bulan'=>$bulan,
                        'active'=>1,
                        'tanggal'=>$request->tanggal,
                        'keterangan_keuangan'=>$request->keterangan_keuangan,

                    ]);
    
                    echo '@ok';
                }else{
                    $save=Keuangan::where('id',$request->id)->update([
                        'status_keuangan_id'=>$request->status_keuangan_id,
                        'nilai'=>$nilai,
                        'nilai_provit'=>0,
                        'tanggal'=>$request->tanggal,
                        'tahun'=>$tahun,
                        'bulan'=>$bulan,
                        'keterangan_keuangan'=>$request->keterangan_keuangan,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
