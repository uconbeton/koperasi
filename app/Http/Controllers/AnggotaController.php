<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\Anggota;
use App\Models\ViewAnggota;

class AnggotaController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('anggota.index');
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('anggota.add',compact('data','id','disabled'));
    }
    public function detail_data(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->first();
        if($id>0){
            $disabled='disabled';
        }else{
            $disabled='';
        }
        return view('anggota.detail_data',compact('data','id','disabled'));
    }

    public function get_data(request $request)
    {
        $data = ViewAnggota::where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                        <span title="Detail" onclick="detail_data(`' . encoder($row['id']) . '`)" class="btn btn-green btn-xs"><i class="fas fa-search"></i></span>
                    </div>
                    ';
                return $btn;
            })
            ->addColumn('pilih', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Pilih" onclick="pilih_data(`' . $row['no_anggota'] . '`,`' . $row['no_register'] . '`,`' . $row['nama'] . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i> Pilih</span>
                     </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action','pilih'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=Anggota::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

            if($request->id==0){
                $rules['no_register'] = 'required';
                $messages['no_register.required'] = 'No register tidak boleh kosong';
            }
            $rules['nama'] = 'required';
            $messages['nama.required'] = 'Nama tidak boleh kosong';

            $rules['jenis_kelamin'] = 'required';
            $messages['jenis_kelamin.required'] = 'jenis kelamin tidak boleh kosong';

            $rules['unit_kerja_id'] = 'required';
            $messages['unit_kerja_id.required'] = 'Unit kerja tidak boleh kosong';

            $rules['alamat'] = 'required';
            $messages['alamat.required'] = 'alamat tidak boleh kosong';

            $rules['tgl_lahir'] = 'required';
            $messages['tgl_lahir.required'] = 'Tanggal Lahir tidak boleh kosong';

            $rules['jabatan_id'] = 'required';
            $messages['jabatan_id.required'] = 'Jabatan tidak boleh kosong';

            $rules['tgl_daftar'] = 'required';
            $messages['tgl_daftar.required'] = 'Tanggal Registrasi tidak boleh kosong';

            if($request->file!=""){
                $rules['file'] = 'required|mimes:jpg,png,gif,jpeg';
                $messages['file.required'] = 'Foto tidak boleh kosong';
            }
            

            $rules['no_handphone'] = 'required';
            $messages['no_handphone.required'] = 'No Handphone tidak boleh kosong';

            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                if($request->id==0){
                    $save=Anggota::create([
                        'no_anggota'=>no_anggota(),
                        'no_register'=>$request->no_register,
                        'nama'=>$request->nama,
                        'alamat'=>$request->alamat,
                        'unit_kerja_id'=>$request->unit_kerja_id,
                        'tgl_lahir'=>$request->tgl_lahir,
                        'jabatan_id'=>$request->jabatan_id,
                        'no_handphone'=>$request->no_handphone,
                        'tgl_daftar'=>$request->tgl_daftar,
                        'jenis_kelamin'=>$request->jenis_kelamin,
                        'active'=>1,
                    ]);
    
                    echo '@ok';
                }else{
                    $save=Anggota::where('id',$request->id)->update([
                        'nama'=>$request->nama,
                        'alamat'=>$request->alamat,
                        'unit_kerja_id'=>$request->unit_kerja_id,
                        'jabatan_id'=>$request->jabatan_id,
                        'tgl_lahir'=>$request->tgl_lahir,
                        'no_handphone'=>$request->no_handphone,
                        'tgl_daftar'=>$request->tgl_daftar,
                        'jenis_kelamin'=>$request->jenis_kelamin,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
