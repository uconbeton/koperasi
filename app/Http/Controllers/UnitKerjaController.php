<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Validator;

use App\Models\MUnitkerja;

class UnitKerjaController extends Controller
{
    //Contoh konfigurasi controller dengan view
    public function index(request $request){
        return view('master.unit_kerja.index');
    }

    public function add(request $request){
        error_reporting(0);
        $id=decoder($request->id);
        $data=MUnitkerja::where('id',$id)->first();
        return view('master.unit_kerja.add',compact('data','id'));
    }

    public function get_data(request $request)
    {
        $data = MUnitkerja::where('active',1)->orderBy('id', 'DESC')->get();

        return  DataTables::of($data)->addIndexColumn()
            ->addColumn('action', function ($row) {
                $btn = '
                    <div class="btn-group">
                        <span title="Ubah" onclick="tambah_data(`' . encoder($row['id']) . '`)" class="btn btn-primary btn-xs"><i class="fas fa-pencil-alt"></i></span>
                        <span title="Detail" onclick="delete_data(`' . encoder($row['id']) . '`)" class="btn btn-danger btn-xs"><i class="fas fa-trash-can"></i></span>
                    </div>
                    ';
                return $btn;
            })
            ->rawColumns([
                'action'
            ])
            ->make(true);
    }

    public function delete_data(request $request){
        $id=decoder($request->id);
        $data=MUnitkerja::where('id',$id)->update(['active'=>0]);
    }

    public function store(request $request){
        $rules = [];
        $messages = [];

        
            $rules['unit_kerja'] = 'required';
            $messages['unit_kerja.required'] = 'Unit Kerja tidak boleh kosong';

            $rules['keterangan_unit_kerja'] = 'required';
            $messages['keterangan_unit_kerja.required'] = 'Keterangan tidak boleh kosong';

            
            $validator = Validator::make($request->all(), $rules, $messages);
            $val = $validator->Errors();

            if ($validator->fails()) {
                echo '<div class="nitof"><b>Oops Error !</b><br><div class="isi-nitof">';
                foreach (parsing_validator($val) as $value) {

                    foreach ($value as $isi) {
                        echo '-&nbsp;' . $isi . '<br>';
                    }
                }
                echo '</div></div>';
            } else {
                if($request->id==0){
                    $save=MUnitkerja::create([
                        'unit_kerja'=>$request->unit_kerja,
                        'keterangan_unit_kerja'=>$request->keterangan_unit_kerja,
                        'active'=>1,
                    ]);
    
                    echo '@ok';
                }else{
                    $save=MUnitkerja::where('id',$request->id)->update([
                        'unit_kerja'=>$request->unit_kerja,
                        'keterangan_unit_kerja'=>$request->keterangan_unit_kerja,
                    ]);
    
                    echo '@ok';
                }
                
            }
        
    }
}
