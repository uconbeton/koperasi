<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MTabelpinjaman extends Model
{
    use HasFactory;

    protected $table = 'm_tabel_pinjaman';
    protected $guarded = ['id'];
    public $timestamps = false;
}
