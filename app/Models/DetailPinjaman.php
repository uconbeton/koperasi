<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPinjaman extends Model
{
    use HasFactory;

    protected $table = 'detail_pinjaman';
    protected $guarded = ['id'];
    public $timestamps = false;
}
