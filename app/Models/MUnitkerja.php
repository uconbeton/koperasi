<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MUnitkerja extends Model
{
    use HasFactory;

    protected $table = 'm_unit_kerja';
    protected $guarded = ['id'];
    public $timestamps = false;
}
