<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewPinjaman extends Model
{
    use HasFactory;

    protected $table = 'view_pinjaman';
    protected $guarded = ['id'];
    public $timestamps = false;
}
