<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MJenissimpanan extends Model
{
    use HasFactory;

    protected $table = 'm_jenis_simpanan';
    protected $guarded = ['id'];
    public $timestamps = false;
}
