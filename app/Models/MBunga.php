<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MBunga extends Model
{
    use HasFactory;

    protected $table = 'm_bunga';
    protected $guarded = ['id'];
    public $timestamps = false;
}
