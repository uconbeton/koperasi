<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewAnggota extends Model
{
    use HasFactory;

    protected $table = 'view_anggota';
    protected $guarded = ['id'];
    public $timestamps = false;
}
