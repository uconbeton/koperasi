<?php

function get_unit_kerja()
{
    $data = App\Models\MUnitkerja::where('active',1)->orderBy('unit_kerja', 'Asc')->get();
    return $data;
}
function get_jabatan()
{
    $data = App\Models\MJabatan::where('active',1)->orderBy('nama_jabatan', 'Asc')->get();
    return $data;
}
function jumlah_anggota($jenis_kelamin)
{
    if($jenis_kelamin==0){
        $data = App\Models\Anggota::where('active',1)->count();
        return $data;
    }else{
        $data = App\Models\Anggota::where('jenis_kelamin',$jenis_kelamin)->where('active',1)->count();
        return $data;
    }
   
}
function get_tabel_pinjaman()
{
    $data = App\Models\MTabelpinjaman::where('active',1)->orderBy('nilai', 'Asc')->get();
    return $data;
}
function persen_kategori($id)
{
    $data = App\Models\MKategori::where('id',$id)->first();
    return $data->persen;
}
function bunga()
{
    $data = App\Models\MBunga::where('tahun',date('Y'))->orderBy('id','Desc')->firstOrfail();
    return $data->persen;
}
function uang_masuk($tahun)
{
    $m = App\Models\Keuangan::where('tahun',$tahun)->where('status_keuangan_id',1)->sum('nilai');
    $b = App\Models\Keuangan::where('tahun',$tahun)->where('status_keuangan_id',1)->sum('nilai_provit');
    return $m+$b;
}
function saldo_simpanan($tipe_id,$tahun)
{
    if($tahun==0){
        $m = App\Models\Keuangan::where('tipe_id',$tipe_id)->where('status_keuangan_id',1)->sum('nilai');
        $b = App\Models\Keuangan::where('tipe_id',$tipe_id)->where('status_keuangan_id',2)->sum('nilai');
        return $m-$b;
    }else{
        $m = App\Models\Keuangan::where('tahun',$tahun)->where('tipe_id',$tipe_id)->where('status_keuangan_id',1)->sum('nilai');
        $b = App\Models\Keuangan::where('tahun',$tahun)->where('tipe_id',$tipe_id)->where('status_keuangan_id',2)->sum('nilai');
        return $m-$b;
    }
    
}
function uang_provit($tahun)
{
    $b = App\Models\Keuangan::where('tahun',$tahun)->where('status_keuangan_id',1)->sum('nilai_provit');
    return $b;
}
function anggota($no_anggota)
{
    $b = App\Models\Anggota::where('no_anggota',$no_anggota)->first();
    return $b;
}
function uang_keluar($tahun)
{
    $b = App\Models\Keuangan::where('tahun',$tahun)->where('status_keuangan_id',2)->sum('nilai');
    return $b;
}
function uang_hutang($tahun)
{
    $b = App\Models\Keuangan::where('tahun',$tahun)->where('status_keuangan_id',3)->sum('nilai');
    return $b;
}
function nilai_simpanan($tipe_id,$no_anggota)
{
    $a = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->where('status_keuangan_id',1)->sum('nilai');
    $b = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->where('status_keuangan_id',2)->sum('nilai');
    return ($a-$b);
}
function simpanan_anggota($tipe_id,$no_anggota)
{
    $a = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->where('status_keuangan_id',1)->sum('nilai');
    $b = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->where('status_keuangan_id',2)->sum('nilai');
    return ($a-$b);
}
function jumlah_simpanan($tipe_id,$no_anggota)
{
    $a = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->where('status_keuangan_id',1)->count();
    return $a;
}
function updatedat_simpanan($tipe_id,$no_anggota)
{
    $cek = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->count();
    if($cek>0){
        $a = App\Models\Simpanan::where('no_anggota',$no_anggota)->where('tipe_id',$tipe_id)->orderBy('id','Desc')->firstOrfail();
        return tanggal_indo_full($a->updated_at);
    }else{
        return 'Null';
    }
    
}

function create_keuangan($no_transaksi,$keterangan_keuangan,$status_keuangan_id,$tipe_id,$nilai,$nilai_provit,$no_anggota,$tanggal)
{
    $exp=explode('-',$tanggal);
    $bulan=$exp[1];
    $tahun=$exp[0];
    $tgl=$exp[2];
    $data = App\Models\Keuangan::UpdateOrcreate([
        'tipe_id'=>$tipe_id,
        'no_transaksi'=>$no_transaksi,
    ],[
        'status_keuangan_id'=>$status_keuangan_id,
        'nilai'=>$nilai,
        'nilai_provit'=>$nilai_provit,
        'no_anggota'=>$no_anggota,
        'tahun'=>$tahun,
        'bulan'=>$bulan,
        'active'=>1,
        'tanggal'=>$tanggal,
        'keterangan_keuangan'=>$keterangan_keuangan,

    ]);
    return '@ok@';
}
