                    <ul class="nav nav-tabs">
						<li class="nav-item">
							<a href="#default-tab-1" data-bs-toggle="tab" class="nav-link active">
								<span class="d-sm-none">Grafik </span>
								<span class="d-sm-block d-none">Grafik </span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#default-tab-2" data-bs-toggle="tab" class="nav-link">
								<span class="d-sm-none">Tab 2</span>
								<span class="d-sm-block d-none">Tab 2</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="#default-tab-3" data-bs-toggle="tab" class="nav-link">
								<span class="d-sm-none">Tab 3</span>
								<span class="d-sm-block d-none">Tab 3</span>
							</a>
						</li>
					</ul>
					<!-- END nav-tabs -->
					<!-- BEGIN tab-content -->
					<div class="tab-content panel rounded-0 p-3 m-0">
						<!-- BEGIN tab-pane -->
						<div class="tab-pane fade active show" id="default-tab-1">
							<h3 class="mt-10px"><i class="fa fa-cog"></i> MODEL {{$model}}</h3>
                            
							<div class="row">
                                <div class="col-md-7">
                                    <div id="apex-area-chart"></div>
                                </div>
                                <div class="col-md-5">
                                
                                </div>
                            </div>
							
						</div>
						<!-- END tab-pane -->
						<!-- BEGIN tab-pane -->
						<div class="tab-pane fade" id="default-tab-2">
							
						</div>
						<!-- END tab-pane -->
						<!-- BEGIN tab-pane -->
						<div class="tab-pane fade" id="default-tab-3">
							
						</div>
						<!-- END tab-pane -->
					</div>
<script src="{{url_plug()}}/assets/plugins/apexcharts/dist/apexcharts.min.js"></script>



<script>
    function load_chart(){
        $.ajax({
            type: 'GET',
            url: "{{url('api/json')}}",
            data: { ide: 1 },
            dataType: 'json',
            beforeSend: function() {
                
            },
            success: function (data) {
                var chart = new ApexCharts(
                        document.querySelector('#apex-area-chart'),
                        data
                );
                chart.render();
                
            }
        });
    }

    $(document).ready(function() {
        load_chart();
       
    

    });



</script>