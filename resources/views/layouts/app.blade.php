
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Managed Tables - Responsive</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN core-css ================== -->
	<link href="{{url_plug()}}/assets/css/vendor.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/css/default/app.min.css" rel="stylesheet" />
	<!-- ================== END core-css ================== -->
	<link href="{{url_plug()}}/assets/plugins/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet" /
	<!-- ================== BEGIN page-css ================== -->
	<link href="{{url_plug()}}/assets/plugins/datatables.net-bs5/css/dataTables.bootstrap5.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/datatables.net-responsive-bs5/css/responsive.bootstrap5.min.css" rel="stylesheet" />
	<!-- ================== END page-css ================== -->
    <link href="{{url_plug()}}/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/ion-rangeslider/css/ion.rangeSlider.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/tag-it/css/jquery.tagit.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/spectrum-colorpicker2/dist/spectrum.min.css" rel="stylesheet" />
	<link href="{{url_plug()}}/assets/plugins/select-picker/dist/picker.min.css" rel="stylesheet" />
	<style>
		#iii{
			margin-bottom: -59px !important;
			/* background: blue !important; */
			border: solid 2px white;
			color: #fff;
			border-radius: 8px;
		}
        table.table-bordered.dataTable td {
            border-bottom-width: 1px;
            padding: 4px 4px 4px 8px;
            vertical-align: top;
        }
        .table-responsive{
            width: 100%;
            overflow-y: hidden;
			min-height: 400px;
			overflow-x: scroll;
        }
		
        .table-responsive-all{
            width: 100%;
			height:400px;
            overflow-x: scroll;
            overflow-y: scroll;
        }
        .swal-text {
            width: 100%;
            color: #000;
        }
        table.table-bordered.dataTable th {
            border-bottom-width: 1px;
            padding: 8px 8px 8px 8px;
            vertical-align: top;
            background:blue;
            color:#fff;
        }
        .col-form-label {
            padding-top: calc(.4375rem + 1px);
            padding-bottom: calc(.4375rem + 1px);
            margin-bottom: 0;
            font-size: inherit;
            font-weight: 600;
            text-align: right;
            line-height: 1.2;
        }
		.modal .modal-header {
			display: flex;
			flex: 1;
			align-items: center;
			background: #59595d;
			color: #fff;
		}
		.loadnya {
			height: 100%;
			width: 0;
			position: fixed;
			z-index: 1070;
			top: 0;
			left: 0;
			background-color: rgb(0,0,0);
			background-color: rgb(243 230 230 / 81%);
			overflow-x: hidden;
			transition: transform .9s;
		}
		.loadnya-content {
			position: relative;
			top: 25%;
			width: 100%;
			text-align: center;
			margin-top: 30px;
			color:#fff;
			font-size:20px;
		}
		.loadnyapage {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1070;
            top: 0;
            left: 0;
            background-color: rgb(243 228 228 / 81%);
            background-color: rgb(243 228 228 / 81%);
            overflow-x: hidden;
            transition: transform .9s;
        }

        .loadnyapage-content {
            position: relative;
            top: 25%;
            width: 100%;
            text-align: center;
            margin-top: 30px;
            color: #fff;
            font-size: 20px;
        }
	</style>
</head>
<body>
	<!-- BEGIN #loader -->
	<div id="loadnya" class="loadnya">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <div class="loadnya-content">
			<button class="btn btn-light" type="button" disabled>
  				Loading...
			</button>
        </div>
	</div>
	<!-- END #loader -->

	<!-- BEGIN #app -->
	<div id="app" class="app app-header-fixed app-sidebar-fixed">
		<!-- BEGIN #header -->
		<div id="header" class="app-header">
			<!-- BEGIN navbar-header -->
			<div class="navbar-header">
				<a href="{{url('/')}}" class="navbar-brand"><span class="navbar-logo"></span> <b class="me-3px">KOPERASI</b> XXXX</a>
				<button type="button" class="navbar-mobile-toggler" data-toggle="app-sidebar-mobile">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- END navbar-header -->
			<!-- BEGIN header-nav -->
			<div class="navbar-nav">
				
				
				<div class="navbar-item navbar-user dropdown">
					<a href="#" class="navbar-link dropdown-toggle d-flex align-items-center" data-bs-toggle="dropdown">
						<img src="{{url_plug()}}/assets/img/user/user-13.jpg" alt="" /> 
						<span>
							<span class="d-none d-md-inline">{{Auth::user()->name}}</span>
							<b class="caret"></b>
						</span>
					</a>
					<div class="dropdown-menu dropdown-menu-end me-1">
						<!-- <a href="extra_profile.html" class="dropdown-item">Edit Profile</a>
						<a href="email_inbox.html" class="dropdown-item d-flex align-items-center">
							Inbox
							<span class="badge bg-danger rounded-pill ms-auto pb-4px">2</span> 
						</a>
						<a href="calendar.html" class="dropdown-item">Calendar</a>
						<a href="extra_settings_page.html" class="dropdown-item">Settings</a>
						<div class="dropdown-divider"></div> -->
						<a href="#" id="logout" class="dropdown-item">Log Out</a>
					</div>
				</div>
			</div>
			<!-- END header-nav -->
		</div>
		<!-- END #header -->
	
		<!-- BEGIN #sidebar -->
		@include('layouts.side')
		<div class="app-sidebar-bg" data-bs-theme="dark"></div>
		<div class="app-sidebar-mobile-backdrop"><a href="#" data-dismiss="app-sidebar-mobile" class="stretched-link"></a></div>
		<!-- END #sidebar -->
		
		<!-- BEGIN #content -->
		@yield('content')
		<!-- END #content -->
		
		<!-- BEGIN theme-panel -->
		<div class="theme-panel">
			<a href="javascript:;" data-toggle="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
			<div class="theme-panel-content" data-scrollbar="true" data-height="100%">
				<h5>App Settings</h5>
				
				<!-- BEGIN theme-list -->
				<div class="theme-list">
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-red" data-theme-class="theme-red" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Red">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-pink" data-theme-class="theme-pink" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Pink">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-orange" data-theme-class="theme-orange" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Orange">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-yellow" data-theme-class="theme-yellow" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Yellow">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-lime" data-theme-class="theme-lime" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Lime">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-green" data-theme-class="theme-green" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Green">&nbsp;</a></div>
					<div class="theme-list-item active"><a href="javascript:;" class="theme-list-link bg-teal" data-theme-class="" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Default">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-cyan" data-theme-class="theme-cyan" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Cyan">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-blue" data-theme-class="theme-blue" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Blue">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-purple" data-theme-class="theme-purple" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Purple">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-indigo" data-theme-class="theme-indigo" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Indigo">&nbsp;</a></div>
					<div class="theme-list-item"><a href="javascript:;" class="theme-list-link bg-black" data-theme-class="theme-gray-600" data-toggle="theme-selector" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-container="body" data-bs-title="Black">&nbsp;</a></div>
				</div>
				<!-- END theme-list -->
				
				<div class="theme-panel-divider"></div>
				
				<div class="row mt-10px">
					<div class="col-8 control-label text-body fw-bold">
						<div>Dark Mode <span class="badge bg-primary ms-1 py-2px position-relative" style="top: -1px;">NEW</span></div>
						<div class="lh-14">
							<small class="text-body opacity-50">
								Pengaturan Thema Warna Body
							</small>
						</div>
					</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-theme-dark-mode" id="appThemeDarkMode" value="1" />
							<label class="form-check-label" for="appThemeDarkMode">&nbsp;</label>
						</div>
					</div>
				</div>
				
				<div class="theme-panel-divider"></div>
				
				<!-- BEGIN theme-switch -->
				<div class="row mt-10px align-items-center">
					<div class="col-8 control-label text-body fw-bold">Header Fixed</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-header-fixed" id="appHeaderFixed" value="1" checked />
							<label class="form-check-label" for="appHeaderFixed">&nbsp;</label>
						</div>
					</div>
				</div>
				<div class="row mt-10px align-items-center">
					<div class="col-8 control-label text-body fw-bold">Header Inverse</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-header-inverse" id="appHeaderInverse" value="1" />
							<label class="form-check-label" for="appHeaderInverse">&nbsp;</label>
						</div>
					</div>
				</div>
				<div class="row mt-10px align-items-center">
					<div class="col-8 control-label text-body fw-bold">Sidebar Fixed</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-sidebar-fixed" id="appSidebarFixed" value="1" checked />
							<label class="form-check-label" for="appSidebarFixed">&nbsp;</label>
						</div>
					</div>
				</div>
				<div class="row mt-10px align-items-center">
					<div class="col-8 control-label text-body fw-bold">Sidebar Grid</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-sidebar-grid" id="appSidebarGrid" value="1" />
							<label class="form-check-label" for="appSidebarGrid">&nbsp;</label>
						</div>
					</div>
				</div>
				<div class="row mt-10px align-items-center">
					<div class="col-8 control-label text-body fw-bold">Gradient Enabled</div>
					<div class="col-4 d-flex">
						<div class="form-check form-switch ms-auto mb-0">
							<input type="checkbox" class="form-check-input" name="app-gradient-enabled" id="appGradientEnabled" value="1" />
							<label class="form-check-label" for="appGradientEnabled">&nbsp;</label>
						</div>
					</div>
				</div>
				<!-- END theme-switch -->
				
				<div class="theme-panel-divider"></div>
				
			</div>
		</div>
		<!-- END theme-panel -->
		<!-- BEGIN scroll-top-btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-theme btn-scroll-to-top" data-toggle="scroll-to-top"><i class="fa fa-angle-up"></i></a>
		<!-- END scroll-top-btn -->
	</div>
	<!-- END #app -->
	
	<!-- ================== BEGIN core-js ================== -->
	<script src="{{url_plug()}}/assets/js/vendor.min.js"></script>
	<script src="{{url_plug()}}/assets/js/app.min.js"></script>
	<!-- ================== END core-js ================== -->
	<script src="{{url_plug()}}/assets/plugins/ionicons/dist/ionicons/ionicons.js"></script>
	<!-- ================== BEGIN page-js ================== -->
	<script src="{{url_plug()}}/assets/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
	<script src="{{url_plug()}}/assets/plugins/datatables.net-responsive-bs5/js/responsive.bootstrap5.min.js"></script>
	<script src="{{url_plug()}}/assets/js/demo/table-manage-responsive.demo.js"></script>
	<script src="{{url_plug()}}/assets/plugins/@highlightjs/cdn-assets/highlight.min.js"></script>
	<script src="{{url_plug()}}/assets/js/demo/render.highlight.js"></script>
    <script src="{{ url_plug() }}/assets/plugins/gritter/js/jquery.gritter.js"></script>
    <script src="{{ url_plug() }}/assets/plugins/sweetalert/dist/sweetalert.min.js"></script>

    <script src="{{ url_plug() }}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/jquery-migrate/dist/jquery-migrate.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/tag-it/js/tag-it.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/clipboard/dist/clipboard.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/spectrum-colorpicker2/dist/spectrum.min.js"></script>
	<script src="{{ url_plug() }}/assets/plugins/select-picker/dist/picker.min.js"></script>
    <!-- <script src="{{ url_plug() }}/assets/js/demo/form-plugins.demo.js"></script> -->
	<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script >
		$(".exselect2").select2();
        $("#datepicker-disabled-past").datepicker({
            format:"yyyy-mm-dd"
        });
		$("#nilai_rupiah").inputmask({'alias': 'currency', prefix: '',allowMinus: false, digits: 0});
        $("#datepicker-lahir-past").datepicker({
            format:"yyyy-mm-dd"
        });
        $("#logout").on("click", function() {
			swal({
                title: "Yakin melakukan logout?",
                text: "Proses logout akan mengluarkan anda dari sistem",
                type: "warning",
                icon: "info",
                showCancelButton: true,
                align:"center",
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }).then((willDelete) => {
                if (willDelete) {
					location.assign("{{url('logout-perform')}}")
                    
                } else {
                    
                }
			});
            
        }) 
    </script>
	@stack('datatable')
	@stack('function-ajax')
	<!-- ================== END page-js ================== -->
</body>
</html>