<div id="sidebar" class="app-sidebar" data-bs-theme="dark">
			<!-- BEGIN scrollbar -->
			<div class="app-sidebar-content" data-scrollbar="true" data-height="100%">
				<!-- BEGIN menu -->
				<div class="menu">
					<div class="menu-profile">
						<a href="javascript:;" class="menu-profile-link" data-toggle="app-sidebar-profile" data-target="#appSidebarProfileMenu">
							<div class="menu-profile-cover with-shadow"></div>
							<div class="menu-profile-image">
								<img src="{{url_plug()}}/assets/img/user/user-13.jpg" alt="" />
							</div>
							<div class="menu-profile-info">
								<div class="d-flex align-items-center">
									<div class="flex-grow-1">
										{{Auth::user()->name}}
									</div>
									<div class="menu-caret ms-auto"></div>
								</div>
								<small>Admin Koperasi</small>
							</div>
						</a>
					</div>
					<div id="appSidebarProfileMenu" class="collapse">
						<div class="menu-item pt-5px">
							<a href="javascript:;" class="menu-link">
								<div class="menu-icon"><i class="fa fa-cog"></i></div>
								<div class="menu-text">Settings</div>
							</a>
						</div>
						<div class="menu-item">
							<a href="javascript:;" class="menu-link">
								<div class="menu-icon"><i class="fa fa-pencil-alt"></i></div>
								<div class="menu-text"> Send Feedback</div>
							</a>
						</div>
						<div class="menu-item pb-5px">
							<a href="javascript:;" class="menu-link">
								<div class="menu-icon"><i class="fa fa-question-circle"></i></div>
								<div class="menu-text"> Helps</div>
							</a>
						</div>
						<div class="menu-divider m-0"></div>
					</div>
					<div class="menu-header">Menu </div>
					<div class="menu-item">
						<a href="{{url('home')}}" class="menu-link">
							<div class="menu-icon">
								<i class="fas fa-home"></i> 
							</div>
							<div class="menu-text">Home</div>
						</a>
					</div>
					<div class="menu-header">Data Master </div>
					<div class="menu-item has-sub @if (Request::is('master') == 1 || Request::is('master/*') == 1) expand @endif">
						<a href="javascript:;" class="menu-link">
							<div class="menu-icon">
								<i class="fa fa-sitemap"></i>							</div>
							<div class="menu-text">Master</div>
							<div class="menu-caret"></div>
						</a>
						<div class="menu-submenu " @if (Request::is('master') == 1 || Request::is('master/*') == 1) style="display: block; box-sizing: border-box;" @endif>
							<div class="menu-item">
								<a href="{{url('master/jabatan')}}" class="menu-link"><div class="menu-text">Jabatan</div></a>
							</div>
							<div class="menu-item">
								<a href="{{url('master/unit_kerja')}}" class="menu-link"><div class="menu-text">Unit Kerja</div></a>
							</div>
							<div class="menu-item">
								<a href="{{url('master/tabel_pinjaman')}}" class="menu-link"><div class="menu-text">Tabel Pinjaman</div></a>
							</div>
							<div class="menu-item">
								<a href="{{url('master/kategori')}}" class="menu-link"><div class="menu-text">Kategori</div></a>
							</div>
						</div>
					</div>
					<div class="menu-header">Data Transaksi</div>
					<div class="menu-item">
						<a href="{{url('anggota')}}" class="menu-link">
							<div class="menu-icon">
								<i class="fa fa-users"></i> 
							</div>
							<div class="menu-text">Anggota</div>
						</a>
					</div>
					<div class="menu-item has-sub @if (Request::is('pinjaman') == 1 || Request::is('pinjaman/*') == 1) expand @endif">
						<a href="javascript:;" class="menu-link">
							<div class="menu-icon">
								<i class="fa fa-money-bill-1-wave"></i>							
							</div>
							<div class="menu-text">pinjaman</div>
							<div class="menu-caret"></div>
						</a>
						<div class="menu-submenu " @if (Request::is('pinjaman') == 1 || Request::is('pinjaman/*') == 1) style="display: block; box-sizing: border-box;" @endif>
							<div class="menu-item">
								<a href="{{url('pinjaman/aktif')}}" class="menu-link"><div class="menu-text">Aktif / Belum Lunas</div></a>
							</div>
							<div class="menu-item">
								<a href="{{url('pinjaman/lunas')}}" class="menu-link"><div class="menu-text">Lunas</div></a>
							</div>
							
						</div>
					</div>
					<div class="menu-item has-sub @if (Request::is('simpanan') == 1 || Request::is('simpanan/*') == 1) expand @endif">
						<a href="javascript:;" class="menu-link">
							<div class="menu-icon">
								<i class="fa fa-bank"></i>							
							</div>
							<div class="menu-text">Simpanan</div>
							<div class="menu-caret"></div>
						</a>
						<div class="menu-submenu " @if (Request::is('simpanan') == 1 || Request::is('simpanan/*') == 1) style="display: block; box-sizing: border-box;" @endif>
							<div class="menu-item">
								<a href="{{url('simpanan')}}?tipe={{encoder(3)}}" class="menu-link"><div class="menu-text">Wajib</div></a>
							</div>
							<div class="menu-item">
								<a href="{{url('simpanan')}}?tipe={{encoder(4)}}" class="menu-link"><div class="menu-text">Pokok</div></a>
							</div>
							
							<div class="menu-item">
								<a href="{{url('simpanan')}}?tipe={{encoder(5)}}" class="menu-link"><div class="menu-text">Sukarela</div></a>
							</div>
							
						</div>
					</div>
					<div class="menu-item">
						<a href="{{url('keuangan')}}" class="menu-link">
							<div class="menu-icon">
								<i class="fas fa-clone"></i> 
							</div>
							<div class="menu-text">Keuangan</div>
						</a>
					</div>
					<div class="menu-header">Setting</div>
					<div class="menu-item">
						<a href="{{url('settingapp')}}" class="menu-link">
							<div class="menu-icon">
								<i class="fas fa-gear"></i> 
							</div>
							<div class="menu-text">Pengaturan</div>
						</a>
					</div>
					
					<!-- BEGIN minify-button -->
					<div class="menu-item d-flex">
						<a href="javascript:;" class="app-sidebar-minify-btn ms-auto d-flex align-items-center text-decoration-none" data-toggle="app-sidebar-minify"><i class="fa fa-angle-double-left"></i></a>
					</div>
					<!-- END minify-button -->
				</div>
				<!-- END menu -->
			</div>
			<!-- END scrollbar -->
		</div>