@extends('layouts.app')
@push('datatable')
<style>
    .hasil_rekap {
        width: 100%;
        background: #f0fee5;
        min-height: 100px;
        border: dotted 3px #9999a3;
        padding:1%;
    }
</style>
<script>
    function show_data() {
        if ($('#data-table-fixed-header').length !== 0) {
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [300],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('anggota/get_data')}}",
                dom: 'lrtip',
                columns: [
                    
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'pilih' },
                    { data: 'no_anggota' },
                    { data: 'no_register' },
                    { data: 'nama' },
                    { data: 'unit_kerja' },
                    { data: 'nama_jabatan' },
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
    };
    

    $(document).ready(function() {
        show_data();

    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active"> Pengajuan Pinjaman</li>
			</ol>
			
			<h1 class="page-header">Form Pengajuan Pinjaman <small></small></h1>
			
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <form  id="mydata" method="post" action="{{ url('pinjaman') }}" enctype="multipart/form-data" >
                                <input type="hidden" name="id" value="{{$id}}">             
                                @csrf
                                <div class="form-group row mb-2">
									<label class="form-label col-form-label col-lg-3">Cari Anggota</label>
									<div class="col-lg-4">
										<div class="input-group input-group-sm date"  >
											<input type="text" name="no_anggota" value="{{$data->no_anggota}}" id="no_anggota" style="background:#e8e8fc" class="form-control" placeholder="" readonly />
											<span class="input-group-text input-group-addon" onclick="cari_data()"><i class="fa fa-search"></i></span>
										</div>
									</div>
								</div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">NIK & Nama *</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." id="no_register" name="nama" value="{{$data->no_register}}" />
                                        
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." id="nama" name="nama" value="{{$data->nama}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Jumlah Pinjaman *</label>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm "  id="tabel_pinjaman_id" name="tabel_pinjaman_id" >
                                            <option value="">.::Pilih------</option>
                                            @foreach(get_tabel_pinjaman() as $o)
                                            <option value="{{$o->id}}" @if($data->tabel_pinjaman_id==$o->id) selected @endif > Rp. {{uang($o->nilai)}} </option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Jumlah Angsuran *</label>
                                    <div class="col-md-3">
                                        <select class="form-control form-control-sm "  onchange="tentukan_hasil(this.value)" id="lama_angsuran" name="lama_angsuran" >
                                            <option value="">.::Pilih------</option>
                                            @for($x=1;$x<11;$x++)
                                                <option value="{{$x}}" @if($data->lama_angsuran==$x) selected @endif >{{$x}}X Angsuran</option>
                                            @endfor
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
									<label class="form-label col-form-label col-lg-3">Tanggal Pinjaman</label>
									<div class="col-lg-3">
										<div class="input-group input-group-sm date" id="datepicker-disabled-past"  >
											<input type="text" name="tgl_pinjam" value="{{$data->tgl_pinjam}}" class="form-control" placeholder="yyyy-mm-dd" />
											<span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Alasan Pinjaman</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control form-control-sm " placeholder="Ketik....." name="keterangan" rows="4"> {{$data->keterangan}}</textarea>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2" >
                                    <label class="form-label col-form-label col-md-3">&nbsp;</label>
                                    <div class="col-md-9">
                                        <div class="hasil_rekap">

                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer m-t-10" style="justify-content: center;padding: 0.2rem;background: #e5e5e9; border-top: solid 1px #f3f3f3;border-radius: 0px;">
                                    <a href="javascript:;" class="btn btn-danger " onclick="location.assign(`{{url('pinjaman')}}`)"><i class="fas fa-arrow-alt-circle-left fa-flip-vertical"></i> Kembali </a>
                                    <a href="javascript:;" class="btn btn-success " onclick="simpan_data()">Simpan <i class="fas fa-arrow-alt-circle-right fa-flip-vertical"></i></a>
                                </div>
                            </form>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>

        <div id="modal-cari" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Daftar Anggota</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 ">
                                   
                                   
                                </div>
                                <div class="col-md-6 ">
                                    <input type="text" id="cari_data" placeholder="Cari............" class="form-control">
                                </div>
                            </div>
                            <div class="table-responsive-all">
                                <table width="100%" id="data-table-fixed-header" class="table table-bordered align-middle">
                                    <thead>
                                        <tr role="row">
                                            <th width="1%">No</th>
                                            <th width="5%"></th>
                                            <th width="8%">No Anggota</th>
                                            <th width="8%">NIK</th>
                                            <th>Nama</th>
                                            <th width="9%">Unit Kerja</th>
                                            <th width="9%">Jabatan</th>
                                        </tr>
                                    </thead>


                                
                                </table>
						    </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div> 
@endsection

@push('function-ajax')
    <script>
        $('.hasil_rekap').hide();
        @if($id==0)
        function cari_data(){
            $('#modal-cari').modal('show');
        }
        @else
            tentukan_hasil({{$data->lama_angsuran}})

        @endif
        function tentukan_hasil(lama_angsuran){
            var tabel_pinjaman_id=$('#tabel_pinjaman_id').val();
            if(tabel_pinjaman_id>0){
                $.ajax({
                    type: 'GET',
                    url: "{{url('pinjaman/tentukan_hasil')}}",
                    data: "lama_angsuran="+lama_angsuran+"&tabel_pinjaman_id="+tabel_pinjaman_id,
                    success: function(msg){
                        $('.hasil_rekap').show();
                        $('.hasil_rekap').html(msg);
                    
                        
                    }
                });
            }else{
                swal("Pilih Jumlah Pinjaman", "", "warning");
                $('#lama_angsuran').val("");
            }
            
        }
        function pilih_data(no_anggota,no_register,nama){
            $('#modal-cari').modal('hide');
            $('#no_anggota').val(no_anggota);
            $('#no_register').val(no_register);
            $('#nama').val(nama);
        }
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('pinjaman') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.assign("{{url('pinjaman')}}")
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
    </script>
@endpush