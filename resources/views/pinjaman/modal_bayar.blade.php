                                <div class="form-group row mb-2">
									<label class="form-label col-form-label col-lg-3">No Peminjaman</label>
									<div class="col-lg-4">
										<div class="input-group input-group-sm date"  >
											<input type="text" name="no_pinjaman" value="{{$data->no_pinjaman}}"  style="background:#e8e8fc" class="form-control" placeholder="" readonly />
										</div>
									</div>
								</div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Yang Mengajukan</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." name="nama" value="{{$data->no_anggota}}" />
                                        
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." name="nama" value="{{$data->nama}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Nilai & Lama Pinjaman</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." value="Rp.{{uang($data->nilai_pinjaman)}}" />
                                        
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." value="{{$data->lama_angsuran}}X" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-3">Tagihan Angsuran</label>
                                    <div class="col-md-5">
                                        <input type="text" class="form-control form-control-sm " disabled placeholder="Ketik....." value="Rp.{{uang($data->nilai)}}" />
                                        
                                    </div>
                                    
                                </div>