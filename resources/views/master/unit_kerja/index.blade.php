@extends('layouts.app')
@push('datatable')
<script>

        
    
    

    $(document).ready(function() {
        
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('master/unit_kerja/get_data')}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    
                    { data: 'unit_kerja' },
                    { data: 'keterangan_unit_kerja' },
                    { data: 'action' }
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        
    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Jabatan</li>
			</ol>
			
			<h1 class="page-header">Master Jabatan <small></small></h1>
			
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <div class="row">
                                <div class="col-md-8 ">
                                   
                                    <span onclick="tambah_data(`{{encoder(0)}}`)" class="btn btn-sm btn-primary mb-3">Tambah Data</span>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <input type="text" id="cari_data" placeholder="Cari............" class="form-control">
                                </div>
                            </div>
                            <table width="100%" id="data-table-fixed-header" class="table table-bordered align-middle">
                                <thead>
                                    <tr role="row">
                                        <th width="1%">No</th>
                                        <th width="35%">Jabatan</th>
                                        <th >Keterangan</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                </thead>
                            </table>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>
@endsection

@push('function-ajax')
    <script>
        function tambah_data(id) {
    
            location.assign("{{ url('master/unit_kerja/add') }}?id="+id)
        }

        function delete_data(id){
                swal({
                        title: "Yakin menghapus data  ini ?",
                        text: "data akan hilang dari daftar  ini",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'GET',
                            url: "{{url('master/unit_kerja/delete_data')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Sukses diproses", "", "success")
                                var table=$('#data-table-fixed-header').DataTable();
                                    table.ajax.url("{{ url('master/unit_kerja/get_data')}}").load();
                              
                            }
                        });
                        
                    } else {
                        var table=$('#data-table-fixed-header').DataTable();
                            table.ajax.url("{{ url('master/unit_kerja/get_data')}}").load();
                    }
                });
                
            
        } 
    </script>
@endpush