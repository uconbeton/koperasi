@extends('layouts.app')
@push('datatable')
<script>
    function show_data() {
        if ($('#data-table-fixed-header').length !== 0) {
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('master/unit_kerja/get_data')}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    
                    { data: 'unit_kerja' },
                    { data: 'keterangan_unit_kerja' },
                    { data: 'action' }
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
    };
    

    $(document).ready(function() {
        show_data();

    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Unit Kerja</li>
			</ol>
			
			<h1 class="page-header">Master Unit Kerja <small></small></h1>
			
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <form  id="mydata" method="post" action="{{ url('master/unit_kerja') }}" enctype="multipart/form-data" >
                                <input type="hidden" name="id" value="{{$id}}">             
                                @csrf
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Nama Unit Kerja *</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm " placeholder="Ketik....." name="unit_kerja" value="{{$data->unit_kerja}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Keterangan *</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control form-control-sm " placeholder="Ketik....." name="keterangan_unit_kerja" value="{{$data->keterangan_unit_kerja}}" />
                                        
                                    </div>
                                </div>
                                <div class="modal-footer m-t-10" style="justify-content: center;padding: 0.2rem;background: #e5e5e9; border-top: solid 1px #f3f3f3;border-radius: 0px;">
                                    <a href="javascript:;" class="btn btn-danger " onclick="location.assign(`{{url('master/unit_kerja')}}`)"><i class="fas fa-arrow-alt-circle-left fa-flip-vertical"></i> Kembali </a>
                                    <a href="javascript:;" class="btn btn-success " onclick="simpan_data()">Simpan <i class="fas fa-arrow-alt-circle-right fa-flip-vertical"></i></a>
                                </div>
                            </form>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>
@endsection

@push('function-ajax')
    <script>
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('master/unit_kerja') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.assign("{{url('master/unit_kerja')}}")
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
    </script>
@endpush