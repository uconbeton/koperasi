@extends('layouts.app')
@push('datatable')
<script>

        
    
    

    $(document).ready(function() {
        
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: true,
                ajax:"{{ url('anggota/get_data')}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    
                    { data: 'no_anggota' },
                    { data: 'no_register' },
                    { data: 'nama' },
                    { data: 'alamat' },
                    { data: 'unit_kerja' },
                    { data: 'nama_jabatan' },
                    { data: 'tgl_daftar' },
                    { data: 'action' }
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        
    });
</script>
@endpush
@section('content')		
        <div id="content" class="app-content">
			<!-- BEGIN breadcrumb -->
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Extra</a></li>
				<li class="breadcrumb-item active">Pengaturan</li>
			</ol>
			<!-- END breadcrumb -->
			<!-- BEGIN page-header -->
			<h1 class="page-header">Pengaturan System</h1>
			<!-- END page-header -->
			<hr class="mb-4">
			<!-- BEGIN row -->
			
			<div class="row">
				<!-- BEGIN col-3 -->
				<div style="width: 230px">
					<!-- BEGIN #sidebar-bootstrap -->
					<nav class="navbar navbar-sticky d-none d-xl-block my-n4 py-4 h-100 text-end">
						<nav class="nav" id="bsSpyTarget">
							<a class="nav-link active" href="#general" data-toggle="scroll-to">User Akun</a>
							<a class="nav-link" href="#notifications" data-toggle="scroll-to">Simpanan Anggota</a>
							<!-- <a class="nav-link" href="#privacyAndSecurity" data-toggle="scroll-to">Privacy and security</a>
							<a class="nav-link" href="#payment" data-toggle="scroll-to">Payment</a>
							<a class="nav-link" href="#shipping" data-toggle="scroll-to">Shipping</a>
							<a class="nav-link" href="#mediaAndFiles" data-toggle="scroll-to">Media and Files</a>
							<a class="nav-link" href="#languages" data-toggle="scroll-to">Languages</a>
							<a class="nav-link" href="#system" data-toggle="scroll-to">System</a>
							<a class="nav-link" href="#resetSettings" data-toggle="scroll-to">Reset settings</a> -->
						</nav>
					</nav>
					<!-- END #sidebar-bootstrap -->
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-9 -->
				<div class="col-xl-8" id="bsSpyContent">
					<!-- BEGIN #general -->
					<div id="general" class="mb-4 pb-3">
						<h4 class="d-flex align-items-center mb-2">
							<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="1em" height="1em" viewBox="0 0 24 24" data-icon="solar:user-bold-duotone" class="iconify fs-24px me-2 text-body text-opacity-75 my-n1 iconify--solar"><circle cx="12" cy="6" r="4" fill="currentColor"></circle><path fill="currentColor" d="M20 17.5c0 2.485 0 4.5-8 4.5s-8-2.015-8-4.5S7.582 13 12 13s8 2.015 8 4.5" opacity=".5"></path></svg> User Akun
						</h4>
						<p>View and update your general account information and settings.</p>
						<div class="card">
							<div class="list-group list-group-flush fw-bold">
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Nama</div>
										<div class="text-body text-opacity-60" id="name">{{Auth::user()->name}}</div>
									</div>
									<div class="w-100px">
										<a href="#" onclick="show_form_user('name','{{Auth::user()->name}}')" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Email</div>
										<div class="text-body text-opacity-60" id="email">{{Auth::user()->email}}</div>
									</div>
									<div class="w-100px">
										<a href="#" onclick="show_form_user('email','{{Auth::user()->email}}')" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Username</div>
										<div class="text-body text-opacity-60" id="username">{{Auth::user()->username}}</div>
									</div>
									<div class="w-100px">
										<a href="#" onclick="show_form_user('username','{{Auth::user()->username}}')" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Password</div>
										<div class="text-body text-opacity-60" id="password">*********************</div>
									</div>
									<div class="w-100px">
										<a href="#" onclick="show_form_user('password','')" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					<!-- END #general -->
				
					<!-- BEGIN #notifications -->
					<div id="notifications" class="mb-4 pb-3">
						<h4 class="d-flex align-items-center mb-2 mt-3">
							<svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" role="img" width="1em" height="1em" viewBox="0 0 24 24" data-icon="solar:bell-bold-duotone" class="iconify fs-24px me-2 text-body text-opacity-75 my-n1 iconify--solar"><path fill="currentColor" d="M18.75 9v.704c0 .845.24 1.671.692 2.374l1.108 1.723c1.011 1.574.239 3.713-1.52 4.21a25.794 25.794 0 0 1-14.06 0c-1.759-.497-2.531-2.636-1.52-4.21l1.108-1.723a4.393 4.393 0 0 0 .693-2.374V9c0-3.866 3.022-7 6.749-7s6.75 3.134 6.75 7" opacity=".5"></path><path fill="currentColor" d="M7.243 18.545a5.002 5.002 0 0 0 9.513 0c-3.145.59-6.367.59-9.513 0"></path></svg> 
							Setting Simpanan Anggota
						</h4>
						<!-- <p>Enable or disable what notifications you want to receive.</p> -->
						<div class="card">
							<div class="list-group list-group-flush fw-bold">
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Comments</div>
										<div class="text-body text-opacity-60 d-flex align-items-center">
											<i class="fa fa-circle fs-6px mt-1px fa-fw text-success me-2"></i> Enabled (Push, SMS)
										</div>
									</div>
									<div>
										<a href="#modalEdit" data-bs-toggle="modal" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Tags</div>
										<div class="text-body text-opacity-60 d-flex align-items-center">
											<i class="fa fa-circle fs-6px mt-1px fa-fw text-body text-opacity-25 me-2"></i> Disabled
										</div>
									</div>
									<div>
										<a href="#modalEdit" data-bs-toggle="modal" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>Reminders</div>
										<div class="text-body text-opacity-60 d-flex align-items-center">
											<i class="fa fa-circle fs-6px mt-1px fa-fw text-success me-2"></i> Enabled (Push, Email, SMS)
										</div>
									</div>
									<div>
										<a href="#modalEdit" data-bs-toggle="modal" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
								<div class="list-group-item d-flex align-items-center">
									<div class="flex-fill">
										<div>New orders</div>
										<div class="text-body text-opacity-60 d-flex align-items-center">
											<i class="fa fa-circle fs-6px mt-1px fa-fw text-success me-2"></i> Enabled (Push, Email, SMS)
										</div>
									</div>
									<div>
										<a href="#modalEdit" data-bs-toggle="modal" class="btn btn-secondary w-100px">Edit</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- END #notifications -->
				
					
					
				</div>
				<!-- END col-9-->
			</div>
			<!-- END row -->
		</div>

        <div id="modal-user" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:40%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydata" method="post" action="{{ url('settingapp/store_user') }}" enctype="multipart/form-data" >
                                    
                            @csrf
                            <!-- <input type="submit"> -->
                            <div id="tampil_form_user"></div>
                        </form>
                        
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Tutup</a>
                        <a href="javascript:;" class="btn btn-primary" onclick="simpan_data()">Simpan</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div> 
@endsection

@push('function-ajax')
    <script>
        function show_form_user(act,valui) {
    
            $('#modal-user').modal('show');
            $('#modal-user .modal-title').html(act+' Pengguna');
            $('#tampil_form_user').html('<input type="hidden" name="tipe" value="'+act+'">'
                        +'<div class="row mb-2">'
                            +'<label class="form-label col-form-label col-md-3 " style="text-transform: capitalize;" >'+act+'</label>'
                            +'<div class="col-md-9">'
                                +'<input type="text" class="form-control form-control-sm " value="'+valui+'" name="value" placeholder="Ketik....."  />'
                                
                            +'</div>'
                        +'</div>');
        }
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('settingapp/store_user') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                location.reload();
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
        function delete_data(id){
                swal({
                        title: "Yakin menghapus data  ini ?",
                        text: "data akan hilang dari daftar  ini",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'GET',
                            url: "{{url('anggota/delete_data')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Sukses diproses", "", "success")
                                var table=$('#data-table-fixed-header').DataTable();
                                    table.ajax.url("{{ url('anggota/get_data')}}").load();
                              
                            }
                        });
                        
                    } else {
                        var table=$('#data-table-fixed-header').DataTable();
                            table.ajax.url("{{ url('anggota/get_data')}}").load();
                    }
                });
                
            
        } 
    </script>
@endpush