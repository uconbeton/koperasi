@extends('layouts.app')

@section('content')		
		<div id="content" class="app-content">
			<!-- BEGIN breadcrumb -->
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- END breadcrumb -->
			<!-- BEGIN page-header -->
			<h1 class="page-header">Dashboard <small>header small text goes here...</small></h1>
			<!-- END page-header -->
			
			<!-- BEGIN row -->
			<div class="d-sm-flex align-items-center mb-3">
				<a href="#" class="btn btn-dark me-2 text-truncate" onclick="filter_data()">
					<i class="fa fa-calendar fa-fw text-white text-opacity-50 ms-n1"></i> 
					<span>Tahun {{$tahun}}</span>
					<b class="caret ms-1 opacity-5"></b>
				</a>
				<div class="text-gray-600 fw-bold mt-2 mt-sm-0">compared to <span id="daterange-prev-date">24 Mar-30 Apr 2023</span></div>
			</div>
			<div class="row">
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Uang Masuk / Bruto</h4>
							<p>Rp.{{uang(uang_masuk($tahun))}}</p>	
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-info">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Pendapatan / Provit</h4>
							<p>Rp.{{uang(uang_provit($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-orange">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Pengeluaran</h4>
							<p>Rp.{{uang(uang_keluar($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Hutang</h4>
							<p>Rp.{{uang(uang_hutang($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
			</div>
			<!-- END row -->
			
			<!-- BEGIN row -->
			<div class="row">
				<!-- BEGIN col-8 -->
				<div class="col-xl-8 ui-sortable">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading ui-sortable-handle">
							<h4 class="panel-title">Website Analytics (Last 7 Days)</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pe-1">
							<div id="interactive-chart" class="h-300px" style="padding: 0px; position: relative;"><canvas class="flot-base" width="945" height="450" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 630.135px; height: 300px;"></canvas><canvas class="flot-overlay" width="945" height="450" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 630.135px; height: 300px;"></canvas><div class="flot-svg" style="position: absolute; top: 0px; left: 0px; height: 100%; width: 100%; pointer-events: none;"><svg style="width: 100%; height: 100%;"><g class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; inset: 0px;"><text x="77.06848684210527" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 15</text><text x="167.40559210526317" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 19</text><text x="257.7426973684211" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 22</text><text x="348.07980263157896" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 25</text><text x="438.4169078947369" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 28</text><text x="528.7540131578947" y="295" class="flot-tick-label tickLabel" style="position: absolute; text-align: center;">May 31</text></g><g class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; inset: 0px;"><text x="14.729166507720947" y="272" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">0</text><text x="7.8645830154418945" y="239.5" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">20</text><text x="7.8645830154418945" y="207" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">40</text><text x="7.8645830154418945" y="174.5" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">60</text><text x="7.8645830154418945" y="142" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">80</text><text x="1" y="12" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">160</text><text x="1" y="109.5" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">100</text><text x="1" y="77" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">120</text><text x="1" y="44.5" class="flot-tick-label tickLabel" style="position: absolute; text-align: right;">140</text></g><g class="" style="position: absolute; inset: 0px;"></g></svg></div><div class="legend" style="position: absolute; top: 18px; right: 31px; width: 116.479px; height: 3.2em; pointer-events: none;"><svg class="legendLayer" style="width:inherit;height:inherit;"><rect class="background" width="100%" height="100%"></rect><defs><symbol id="line" fill="none" viewBox="-5 -5 25 25"><polyline points="0,15 5,5 10,10 15,0"></polyline></symbol><symbol id="area" stroke-width="1" viewBox="-5 -5 25 25"><polyline points="0,15 5,5 10,10 15,0, 15,15, 0,15"></polyline></symbol><symbol id="bars" stroke-width="1" viewBox="-5 -5 25 25"><polyline points="1.5,15.5 1.5,12.5, 4.5,12.5 4.5,15.5 6.5,15.5 6.5,3.5, 9.5,3.5 9.5,15.5 11.5,15.5 11.5,7.5 14.5,7.5 14.5,15.5 1.5,15.5"></polyline></symbol><symbol id="circle" viewBox="-5 -5 25 25"><circle cx="0" cy="15" r="2.5"></circle><circle cx="5" cy="5" r="2.5"></circle><circle cx="10" cy="10" r="2.5"></circle><circle cx="15" cy="0" r="2.5"></circle></symbol><symbol id="rectangle" viewBox="-5 -5 25 25"><rect x="-2.1" y="12.9" width="4.2" height="4.2"></rect><rect x="2.9" y="2.9" width="4.2" height="4.2"></rect><rect x="7.9" y="7.9" width="4.2" height="4.2"></rect><rect x="12.9" y="-2.1" width="4.2" height="4.2"></rect></symbol><symbol id="diamond" viewBox="-5 -5 25 25"><path d="M-3,15 L0,12 L3,15, L0,18 Z"></path><path d="M2,5 L5,2 L8,5, L5,8 Z"></path><path d="M7,10 L10,7 L13,10, L10,13 Z"></path><path d="M12,0 L15,-3 L18,0, L15,3 Z"></path></symbol><symbol id="cross" fill="none" viewBox="-5 -5 25 25"><path d="M-2.1,12.9 L2.1,17.1, M2.1,12.9 L-2.1,17.1 Z"></path><path d="M2.9,2.9 L7.1,7.1 M7.1,2.9 L2.9,7.1 Z"></path><path d="M7.9,7.9 L12.1,12.1 M12.1,7.9 L7.9,12.1 Z"></path><path d="M12.9,-2.1 L17.1,2.1 M17.1,-2.1 L12.9,2.1 Z"></path></symbol><symbol id="plus" fill="none" viewBox="-5 -5 25 25"><path d="M0,12 L0,18, M-3,15 L3,15 Z"></path><path d="M5,2 L5,8 M2,5 L8,5 Z"></path><path d="M10,7 L10,13 M7,10 L13,10 Z"></path><path d="M15,-3 L15,3 M12,0 L18,0 Z"></path></symbol></defs><g><use xlink:href="#line" class="legendIcon" x="3px" y="0em" stroke="#348fe2" stroke-width="2" width="1.5em" height="1.5em"></use><use xlink:href="#circle" class="legendIcon" x="3px" y="0em" fill="#FFFFFF" stroke="#348fe2" stroke-width="2" width="1.5em" height="1.5em"></use><text x="3px" y="0em" text-anchor="start"><tspan dx="2em" dy="1.2em">Page Views</tspan></text></g><g><use xlink:href="#line" class="legendIcon" x="3px" y="1.5em" stroke="#32a932" stroke-width="2" width="1.5em" height="1.5em"></use><use xlink:href="#circle" class="legendIcon" x="3px" y="1.5em" fill="#FFFFFF" stroke="#32a932" stroke-width="2" width="1.5em" height="1.5em"></use><text x="3px" y="1.5em" text-anchor="start"><tspan dx="2em" dy="1.2em">Visitors</tspan></text></g></svg></div></div>
						</div>
					</div>
					
				</div>
				<!-- END col-8 -->
				<!-- BEGIN col-4 -->
				<div class="col-xl-4 ui-sortable">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse" data-sortable-id="index-6">
						<div class="panel-heading ui-sortable-handle">
							<h4 class="panel-title">Analytics Details</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-panel align-middle mb-0">
								<thead>
									<tr>	
										<th>Source</th>
										<th>Total</th>
										<th>Trend</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td nowrap=""><label class="badge bg-danger">Unique Visitor</label></td>
										<td>13,203 <span class="text-success"><i class="fa fa-arrow-up"></i></span></td>
										<td><div id="sparkline-unique-visitor"><canvas width="64" height="23" style="display: inline-block; width: 64.8229px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
									<tr>
										<td nowrap=""><label class="badge bg-warning">Bounce Rate</label></td>
										<td>28.2%</td>
										<td><div id="sparkline-bounce-rate"><canvas width="80" height="23" style="display: inline-block; width: 80.8542px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
									<tr>
										<td nowrap=""><label class="badge bg-success">Total Page Views</label></td>
										<td>1,230,030</td>
										<td><div id="sparkline-total-page-views"><canvas width="92" height="23" style="display: inline-block; width: 92.8438px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
									<tr>
										<td nowrap=""><label class="badge bg-blue">Avg Time On Site</label></td>
										<td>00:03:45</td>
										<td><div id="sparkline-avg-time-on-site"><canvas width="100" height="23" style="display: inline-block; width: 100.948px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
									<tr>
										<td nowrap=""><label class="badge bg-default text-gray-900">% New Visits</label></td>
										<td>40.5%</td>
										<td><div id="sparkline-new-visits"><canvas width="106" height="23" style="display: inline-block; width: 106.052px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
									<tr>
										<td nowrap=""><label class="badge bg-gray-900">Return Visitors</label></td>
										<td>73.4%</td>
										<td><div id="sparkline-return-visitors"><canvas width="109" height="23" style="display: inline-block; width: 109.135px; height: 23px; vertical-align: top;"></canvas></div></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
				</div>
				<!-- END col-4 -->
			</div>
			<!-- END row -->
		</div>
@endsection