@extends('layouts.app')
@push('datatable')
<script>

        
    
    

    $(document).ready(function() {
        
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: true,
                ajax:"{{ url('keuangan/get_data')}}?tahun={{$tahun}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'action' },
                    { data: 'status_keuangannya' },
                    { data: 'no_transaksi' }, 
                    { data: 'keterangan_keuangan' },
                    { data: 'nilai' },
                    { data: 'nilai_provit' },
                    { data: 'tanggal' },
                    { data: 'bulan' },
                    { data: 'tahun' },
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        
    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Keuangan</li>
			</ol>
			
			<h1 class="page-header">Daftar Keuangan <small></small></h1>
            <div class="d-sm-flex align-items-center mb-3">
				<a href="#" class="btn btn-dark me-2 text-truncate" onclick="filter_data()">
					<i class="fa fa-calendar fa-fw text-white text-opacity-50 ms-n1"></i> 
					<span>Tahun {{$tahun}}</span>
					<b class="caret ms-1 opacity-5"></b>
				</a>
				<div class="text-gray-600 fw-bold mt-2 mt-sm-0">compared to <span id="daterange-prev-date">24 Mar-30 Apr 2023</span></div>
			</div>
			<div class="row">
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Uang Masuk / Bruto</h4>
							<p>Rp.{{uang(uang_masuk($tahun))}}</p>	
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-info">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Pendapatan / Provit</h4>
							<p>Rp.{{uang(uang_provit($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-orange">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Pengeluaran</h4>
							<p>Rp.{{uang(uang_keluar($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-3 col-md-6">
					<div class="widget widget-stats bg-red">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Hutang</h4>
							<p>Rp.{{uang(uang_hutang($tahun))}}</p>
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
			</div>
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <div class="row">
                                <div class="col-md-8 ">
                                   
                                    <span onclick="tambah_data(`{{encoder(0)}}`)" class="btn btn-sm btn-primary mb-3">Tambah Data</span>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <input type="text" id="cari_data" placeholder="Cari............" class="form-control">
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table width="140%" id="data-table-fixed-header" class="table table-bordered align-middle">
                                    <thead>
                                        <tr role="row">
                                            <th width="1%">No</th>
                                            <th width="4%">Action</th> 
                                            <th width="5%">Status</th> 
                                            <th width="10%">No Transaksi</th>
                                            <th >Keterangan</th>
                                            <th width="9%">Nilai</th>
                                            <th width="9%">Provit</th>
                                            <th width="8%">Tanggal</th>
                                            <th width="8%">Bulan</th>
                                            <th width="8%">Tahun</th>
                                            
                                        </tr>
                                    </thead>
                                    
                                
                                </table>
						    </div>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>
        <div id="modal-filter" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:30%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Filter Tahun</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-2">
                            <label class="form-label  col-md-12">Pilih Tahun Pencarian</label>
                            <div class="col-md-12">
                                <select class="form-control form-control-sm "   id="tahun"  >
                                    <option value="">.::Pilih------</option>
                                    @for($x=2022;$x<(date('Y')+1);$x++)
                                        <option value="{{$x}}" @if($tahun==$x) selected @endif >{{$x}}</option>
                                    @endfor
                                </select>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Tutup</a>
                        <a href="javascript:;" class="btn btn-blue" onclick="proses_cari()">Cari</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <div id="modal-form" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:60%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Keuangan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydata" method="post" action="{{ url('keuangan/') }}" enctype="multipart/form-data" >
                                 
                            @csrf
                            <div id="tampil_form"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Tutup</a>
                        <a href="javascript:;" class="btn btn-blue" onclick="simpan_data()">Simpan</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
@endsection

@push('function-ajax')
    <script>
        function tambah_data(id) {
    
            $('#modal-form').modal('show');
            $('#tampil_form').load("{{url('keuangan/add')}}?id="+id);
        }
        function proses_cari() {
            var tahun=$('#tahun').val();
            location.assign("{{ url('keuangan') }}?tahun="+tahun)
        }
        function filter_data() {
    
           $('#modal-filter').modal('show');
        }
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('keuangan') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.assign("{{url('keuangan')}}")
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
        function delete_data(id){
                swal({
                        title: "Yakin menghapus data  ini ?",
                        text: "data akan hilang dari daftar  ini",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'GET',
                            url: "{{url('anggota/delete_data')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Sukses diproses", "", "success")
                                var table=$('#data-table-fixed-header').DataTable();
                                    table.ajax.url("{{ url('keuangan/get_data')}}").load();
                              
                            }
                        });
                        
                    } else {
                        var table=$('#data-table-fixed-header').DataTable();
                            table.ajax.url("{{ url('keuangan/get_data')}}").load();
                    }
                });
                
            
        } 
    </script>
@endpush