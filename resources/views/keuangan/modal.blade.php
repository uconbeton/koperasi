
                                <input type="hidden" name="id" value="{{$id}}">             
                                @csrf
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-4">Status Keuangan *</label>
                                    <div class="col-md-5">
                                        <select class="form-control form-control-sm "  name="status_keuangan_id" >
                                            <option value="">.::Pilih------</option>
                                            <option value="1" @if($data->status_keuangan_id==1) selected @endif >Keuangan Masuk</option>
                                            <option value="2" @if($data->status_keuangan_id==2) selected @endif >Keuangan Keluar</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-4">Keterangan *</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control form-control-sm " placeholder="Ketik....." name="keterangan_keuangan" rows="4"> {{$data->keterangan_keuangan}}</textarea>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-4">Nilai *</label>
                                    <div class="col-md-5">
                                    <input type="text" class="form-control form-control-sm " placeholder="Ketik....." name="nilai" value="{{$data->nilai}}" id="nilai_rupiah"  />
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
									<label class="form-label col-form-label col-lg-4">Tanggal Transaksi </label>
									<div class="col-lg-4">
										<div class="input-group input-group-sm date" id="datepicker-lahir-past"  >
											<input type="text" name="tanggal" value="{{$data->tanggal}}" class="form-control" placeholder="Select Date" />
											<span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
<script>
    $("#nilai_rupiah").inputmask({'alias': 'currency', prefix: '',allowMinus: false, digits: 0});
    $("#datepicker-lahir-past").datepicker({
            format:"yyyy-mm-dd"
        });
</script>
                               