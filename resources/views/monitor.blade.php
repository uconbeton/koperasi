@extends('layouts.app')
@push('function-ajax')
	<script>
		function load_model(){
			$.ajax({
                type: 'GET',
                url: "{{url('api/api')}}",
                data: { ide: 1 },
                dataType: 'json',
				beforeSend: function() {
					
				},
                success: function (data) {
					$('#model_1').html(data.model_1);
					$('#model_2').html(data.model_2);
					$('#model_3').html(data.model_3);
					$('#model_4').html(data.model_4);

				}
			});

		}
		
		
		function run_model_1(nil){
			$.ajax({
                type: 'GET',
                url: "{{url('api/api')}}?value_model_1="+nil,
                data: { ide: 1 },
                dataType: 'json',
				beforeSend: function() {
					
				},
                success: function (data) {
					$('#model_1').html(data.model_1);
					$('#tampil-log').load("{{url('log')}}?model=1");

				}
			});
		}
		function run_model_2(nil){
			$.ajax({
                type: 'GET',
                url: "{{url('api/api')}}?value_model_2="+nil,
                data: { ide: 1 },
                dataType: 'json',
				beforeSend: function() {
					
				},
                success: function (data) {
					$('#model_2').html(data.model_2);
					$('#tampil-log').load("{{url('log')}}?model=2");
				}
			});
		}
		function run_model_3(nil){
			$.ajax({
                type: 'GET',
                url: "{{url('api/api')}}?value_model_3="+nil,
                data: { ide: 1 },
                dataType: 'json',
				beforeSend: function() {
					
				},
                success: function (data) {
					$('#model_3').html(data.model_3);
					$('#tampil-log').load("{{url('log')}}?model=3");

				}
			});
		}
		function run_model_4(nil){
			$.ajax({
                type: 'GET',
                url: "{{url('api/api')}}?value_model_4="+nil,
                data: { ide: 1 },
                dataType: 'json',
				beforeSend: function() {
					
				},
                success: function (data) {
					$('#model_4').html(data.model_4);
					$('#tampil-log').load("{{url('log')}}?model=4");

				}
			});
		}
		$(document).ready(function() {
			load_model();

		});
	</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			<!-- BEGIN breadcrumb -->
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Tables</a></li>
				<li class="breadcrumb-item"><a href="javascript:;">Managed Tables</a></li>
				<li class="breadcrumb-item active">Responsive</li>
			</ol>
			<!-- END breadcrumb -->
			<!-- BEGIN page-header -->
			<h1 class="page-header">Distributed Control System (DCS)</h1>
			<div class="row gx-3">
				<div class="col-lg-3 text-white col-md-3 col-sm-4 col-12 text-center pb-3 " id="model_1">
					<i class="fas fa-spinner fs-40px h-150px d-flex align-items-center justify-content-center bg-light mb-5" id="iii"></i> 
				</div>
				<div class="col-lg-3 text-white col-md-3 col-sm-4 col-12 text-center pb-3" id="model_2">
					<i class="fas fa-spinner fs-40px h-150px d-flex align-items-center justify-content-center bg-light mb-5" id="iii"></i> 
				</div>
				<div class="col-lg-3 text-white col-md-3 col-sm-4 col-12 text-center pb-3" id="model_3">
					<i class="fas fa-spinner fs-40px h-150px d-flex align-items-center justify-content-center bg-light mb-5" id="iii"></i> 
					
				</div>
				<div class="col-lg-3 text-white col-md-3 col-sm-4 col-12 text-center pb-3" id="model_4">
					<i class="fas fa-spinner fs-40px h-150px d-flex align-items-center justify-content-center bg-light mb-5" id="iii"></i> 
					
				</div>
			</div>
			<div id="tampil-log"></div>
		</div>
@endsection