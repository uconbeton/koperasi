@extends('layouts.app')
@push('datatable')
<script>
    function show_data() {
        if ($('#data-table-fixed-header').length !== 0) {
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('anggota/get_data')}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    
                    { data: 'unit_kerja' },
                    { data: 'keterangan_unit_kerja' },
                    { data: 'action' }
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
    };
    

    $(document).ready(function() {
        show_data();

    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Anggota</li>
			</ol>
			
			<h1 class="page-header">Form Registrasi Anggota <small></small></h1>
			
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <form  id="mydata" method="post" action="{{ url('anggota') }}" enctype="multipart/form-data" >
                                <input type="hidden" name="id" value="{{$id}}">             
                                @csrf
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">NIK (No Register) *</label>
                                    <div class="col-md-3">
                                        <input type="text" {{$disabled}} class="form-control form-control-sm " placeholder="Ketik....." name="no_register" value="{{$data->no_register}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Nama *</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control form-control-sm " placeholder="Ketik....." name="nama" value="{{$data->nama}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Jenis Kelamin *</label>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm "  name="jenis_kelamin" >
                                            <option value="">.::Pilih------</option>
                                            <option value="L" @if($data->jenis_kelamin=='L') selected @endif >Laki - Laki</option>
                                            <option value="P" @if($data->jenis_kelamin=='P') selected @endif >Perempuan</option>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Unit Kerja *</label>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm "  name="unit_kerja_id" >
                                            <option value="">.::Pilih------</option>
                                            @foreach(get_unit_kerja() as $o)
                                            <option value="{{$o->id}}" @if($data->unit_kerja_id==$o->id) selected @endif > {{$o->unit_kerja}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
									<label class="form-label col-form-label col-lg-2">Tanggal Lahir </label>
									<div class="col-lg-4">
										<div class="input-group input-group-sm date" id="datepicker-lahir-past"  >
											<input type="text" name="tgl_lahir" value="{{$data->tgl_lahir}}" class="form-control" placeholder="Select Date" />
											<span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Alamat Lengkap *</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control form-control-sm " placeholder="Ketik....." name="alamat" rows="4"> {{$data->alamat}}</textarea>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Jabatan *</label>
                                    <div class="col-md-4">
                                        <select class="form-control form-control-sm "  name="jabatan_id" >
                                            <option value="">.::Pilih------</option>
                                            @foreach(get_jabatan() as $o)
                                            <option value="{{$o->id}}" @if($data->jabatan_id==$o->id) selected @endif > {{$o->nama_jabatan}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">Foto Anggota</label>
                                    <div class="col-md-8">
                                        <input type="file" class="form-control form-control-sm " placeholder="Ketik....." name="file" value="{{$data->file}}" />
                                        
                                    </div>
                                </div>
                                <div class="row mb-2">
                                    <label class="form-label col-form-label col-md-2">No Handphone *</label>
                                    <div class="col-md-5">
                                        <input type="number" class="form-control form-control-sm " placeholder="Ketik....." name="no_handphone" value="{{$data->no_handphone}}" />
                                        
                                    </div>
                                </div>
                                <div class="form-group row">
									<label class="form-label col-form-label col-lg-2">Tanggal Register</label>
									<div class="col-lg-4">
										<div class="input-group input-group-sm date" id="datepicker-disabled-past"  >
											<input type="text" name="tgl_daftar" value="{{$data->tgl_daftar}}" class="form-control" placeholder="Select Date" />
											<span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
                                
                                <div class="modal-footer m-t-10" style="justify-content: center;padding: 0.2rem;background: #e5e5e9; border-top: solid 1px #f3f3f3;border-radius: 0px;">
                                    <a href="javascript:;" class="btn btn-danger " onclick="location.assign(`{{url('anggota')}}`)"><i class="fas fa-arrow-alt-circle-left fa-flip-vertical"></i> Kembali </a>
                                    <a href="javascript:;" class="btn btn-success " onclick="simpan_data()">Simpan <i class="fas fa-arrow-alt-circle-right fa-flip-vertical"></i></a>
                                </div>
                            </form>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>
@endsection

@push('function-ajax')
    <script>
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('anggota') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.assign("{{url('anggota')}}")
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
    </script>
@endpush