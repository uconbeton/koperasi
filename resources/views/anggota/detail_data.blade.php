@extends('layouts.app')
@push('datatable')
<style>
    .hasil_rekap {
        width: 100%;
        background: #f0fee5;
        min-height: 100px;
        border: dotted 3px #9999a3;
        padding:1%;
    }
</style>
<script>
    function show_data() {
        if ($('#data-table-fixed-header').length !== 0) {
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [300],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('simpanan/get_data_detail')}}?no_anggota={{$data->no_anggota}}&tipe_id=3",
                dom: 'lrtip',
                columns: [
                    
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'tgl_simpanan' },
                    { data: 'nilai' },
                    { data: 'bulan' },
                    { data: 'tahun' },
                    
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
        if ($('#data-table-fixed-header2').length !== 0) {
            var table=$('#data-table-fixed-header2').DataTable({
                lengthMenu: [300],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('simpanan/get_data_detail')}}?no_anggota={{$data->no_anggota}}&tipe_id=4",
                dom: 'lrtip',
                columns: [
                    
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'tgl_simpanan' },
                    { data: 'nilai' },
                    { data: 'bulan' },
                    { data: 'tahun' },
                    
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data2').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
        if ($('#data-table-fixed-header3').length !== 0) {
            var table=$('#data-table-fixed-header3').DataTable({
                lengthMenu: [300],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: false,
                ajax:"{{ url('simpanan/get_data_detail')}}?no_anggota={{$data->no_anggota}}&tipe_id=5",
                dom: 'lrtip',
                columns: [
                    
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'tgl_simpanan' },
                    { data: 'nilai' },
                    { data: 'bulan' },
                    { data: 'tahun' },
                    
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data3').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        }
    };
    

    $(document).ready(function() {
        show_data();

    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active"> Profil Anggota</li>
			</ol>
			
			<h1 class="page-header">Profil Anggota / #{{$data->no_anggota}}<small></small></h1>
			
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <form  id="mydata" method="post" action="{{ url('pinjaman') }}" enctype="multipart/form-data" >
                                <input type="hidden" name="id" value="{{$id}}">             
                                @csrf
                                
                                <div class="row mb-2" >
                                    <div class="col-md-4">
                                        <table class="table table-striped mb-0">
                                            <thead>
                                                <tr>
                                                    <th colspan="3">INFORMASI ANGGOTA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td width="34%">No Anggota</td>
                                                    <td width="5%">:</td>
                                                    <td>{{$data->no_anggota}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama</td>
                                                    <td>:</td>
                                                    <td>{{$data->nama}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tgl Register</td>
                                                    <td>:</td>
                                                    <td>{{$data->tgl_daftar}}</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <!-- BEGIN col-3 -->
                                            <div class="col-xl-4 col-md-6">
                                                <div class="widget widget-stats bg-blue">
                                                    <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                                                    <div class="stats-info">
                                                        <h4>Simpanan Wajib</h4>
                                                        <p>Rp.{{uang(simpanan_anggota(3,$data->no_anggota))}}</p>	
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- END col-4 -->
                                            <!-- BEGIN col-4 -->
                                            <div class="col-xl-4 col-md-6">
                                                <div class="widget widget-stats bg-info">
                                                    <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                                                    <div class="stats-info">
                                                        <h4>Simpanan Pokok</h4>
                                                        <p>Rp.{{uang(simpanan_anggota(4,$data->no_anggota))}}</p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <!-- BEGIN col-4 -->
                                            <div class="col-xl-4 col-md-6">
                                                <div class="widget widget-stats bg-orange">
                                                    <div class="stats-icon"><i class="fa fa-desktop"></i></div>
                                                    <div class="stats-info">
                                                        <h4>Simpanan Sukarela</h4>
                                                        <p>Rp.{{uang(simpanan_anggota(5,$data->no_anggota))}}</p>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal-footer m-t-30" style="justify-content: center;padding: 0.2rem;background: #e5e5e9; border-top: solid 1px #f3f3f3;border-radius: 0px;">
                                    <a href="javascript:;" class="btn btn-danger " onclick="location.assign(`{{url('pinjaman')}}`)"><i class="fas fa-arrow-alt-circle-left fa-flip-vertical"></i> Kembali </a>
                                </div>
                            </form>
						</div>
						<div class="panel-body" style="background: #efefff;">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a href="#default-tab-1" data-bs-toggle="tab" class="nav-link active">
                                            <span class="d-sm-none"><i class="fa fa-calendar"></i> Simpanan Wajib</span>
                                            <span class="d-sm-block d-none"><i class="fa fa-calendar"></i> Simpanan Wajib</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-2" data-bs-toggle="tab" class="nav-link ">
                                            <span class="d-sm-none"><i class="fa fa-calendar"></i> Simpanan Pokok</span>
                                            <span class="d-sm-block d-none"><i class="fa fa-calendar"></i> Simpanan Pokok</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#default-tab-3" data-bs-toggle="tab" class="nav-link ">
                                            <span class="d-sm-none"><i class="fa fa-calendar"></i> Simpanan Sukarela</span>
                                            <span class="d-sm-block d-none"><i class="fa fa-calendar"></i> Simpanan Sukarela</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                                <!-- END nav-tabs -->
                                <!-- BEGIN tab-content -->
                                <div class="tab-content panel rounded-0 p-3 m-0">
                                    <!-- BEGIN tab-pane -->
                                    <div class="tab-pane fade active show" id="default-tab-1">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                            
                                            
                                            </div>
                                            <div class="col-md-6 ">
                                                <input type="text" id="cari_data" placeholder="Cari............" class="form-control">
                                            </div>
                                        </div>
                                        <div class="table-responsive-all">
                                            <table width="100%" id="data-table-fixed-header" class="table table-bordered align-middle">
                                                <thead>
                                                    <tr role="row">
                                                        <th width="1%">No</th>
                                                        <th width="8%">Tanggal</th>
                                                        <th>Nilai</th>
                                                        
                                                        <th width="10%">Bulan</th>
                                                        <th width="8%">Tahun</th>
                                                    </tr>
                                                </thead>
                                                
                                            
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane  show" id="default-tab-2">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                            
                                            
                                            </div>
                                            <div class="col-md-6 ">
                                                <input type="text" id="cari_data2" placeholder="Cari............" class="form-control">
                                            </div>
                                        </div>
                                        <div class="table-responsive-all">
                                            <table width="100%" id="data-table-fixed-header2" class="table table-bordered align-middle">
                                                <thead>
                                                    <tr role="row">
                                                        <th width="1%">No</th>
                                                        <th width="8%">Tanggal</th>
                                                        <th>Nilai</th>
                                                        
                                                        <th width="10%">Bulan</th>
                                                        <th width="8%">Tahun</th>
                                                    </tr>
                                                </thead>
                                                
                                            
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane  show" id="default-tab-3">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                            
                                            
                                            </div>
                                            <div class="col-md-6 ">
                                                <input type="text" id="cari_data3" placeholder="Cari............" class="form-control">
                                            </div>
                                        </div>
                                        <div class="table-responsive-all">
                                            <table width="100%" id="data-table-fixed-header3" class="table table-bordered align-middle">
                                                <thead>
                                                    <tr role="row">
                                                        <th width="1%">No</th>
                                                        <th width="8%">Tanggal</th>
                                                        <th>Nilai</th>
                                                        
                                                        <th width="10%">Bulan</th>
                                                        <th width="8%">Tahun</th>
                                                    </tr>
                                                </thead>
                                                
                                            
                                            </table>
                                        </div>
                                    </div>
                                </div>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>

        <div id="modal-bayar" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:50%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Pembayaran Angsuran</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <form  id="mydatabayar" method="post" action="{{ url('pinjaman/proses_bayar') }}" enctype="multipart/form-data" >
                            @csrf
                            <div id="tampil-bayar"></div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div> 
@endsection

@push('function-ajax')
    <script>
       
        function tampil_bayar(id){
            $('#modal-bayar').modal('show');
            $('#tampil-bayar').load("{{url('pinjaman/modal_bayar')}}?id="+id);
        }
        
        function tentukan_hasil(lama_angsuran){
            var tabel_pinjaman_id="{{$data->tabel_pinjaman_id}}";
            if(tabel_pinjaman_id>0){
                $.ajax({
                    type: 'GET',
                    url: "{{url('pinjaman/tentukan_hasil')}}",
                    data: "lama_angsuran="+lama_angsuran+"&tabel_pinjaman_id="+tabel_pinjaman_id,
                    success: function(msg){
                        $('.hasil_rekap').show();
                        $('.hasil_rekap').html(msg);
                    
                        
                    }
                });
            }else{
                swal("Pilih Jumlah Pinjaman", "", "warning");
                $('#lama_angsuran').val("");
            }
            
        }
        function pilih_data(no_anggota,no_register,nama){
            $('#modal-cari').modal('hide');
            $('#no_anggota').val(no_anggota);
            $('#no_register').val(no_register);
            $('#nama').val(nama);
        }
        function simpan_data(){
            var form=document.getElementById('mydata');
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('pinjaman') }}",
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.assign("{{url('pinjaman')}}")
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
        }
    </script>
@endpush