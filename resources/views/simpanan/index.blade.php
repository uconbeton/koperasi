@extends('layouts.app')
@push('datatable')
<style>
    .select2-container {
        box-sizing: border-box;
        display: inline-block;
        margin: 0;
        position: relative;
        vertical-align: middle;
        z-index: 50000 !important;
    }
</style>
<script>

        
    
    

    $(document).ready(function() {
        
            var table=$('#data-table-fixed-header').DataTable({
                lengthMenu: [20, 40, 60],
                lengthChange:false,
                fixedHeader: {
                    header: true,
                    headerOffset: $('#header').height()
                },
                responsive: true,
                ajax:"{{ url('simpanan/get_data')}}?tipe_id={{encoder($tipe_id)}}",
                dom: 'lrtip',
                columns: [
                    { data: 'id', render: function (data, type, row, meta) 
                        {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        } 
                    },
                    { data: 'selectdata' },
                    { data: 'action' },
                    { data: 'no_anggota' },
                    { data: 'nama' },
                    { data: 'saldo' },
                    { data: 'jumlah_simpanan' },
                    { data: 'updatedat' },
                    
                ],
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '<< previous',
                        next: 'Next>>'
                    }
                }
            });
            $('#cari_data').keyup(function(){
                table.search($(this).val()).draw() ;
            })
        
    });
</script>
@endpush
@section('content')		
		<div id="content" class="app-content">
			
			<ol class="breadcrumb float-xl-end">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">{{$data->tipe}} Anggota</li>
			</ol>
			
			<h1 class="page-header">Daftar {{$data->tipe}} Anggota <small></small></h1>
			<div class="d-sm-flex align-items-center mb-3">
				<a href="#" class="btn btn-dark me-2 text-truncate" onclick="filter_data()">
					<i class="fa fa-calendar fa-fw text-white text-opacity-50 ms-n1"></i> 
					<span>Tahun {{$tahun}}</span>
					<b class="caret ms-1 opacity-5"></b>
				</a>
				<div class="text-gray-600 fw-bold mt-2 mt-sm-0">compared to <span id="daterange-prev-date">24 Mar-30 Apr 2023</span></div>
			</div>
			<div class="row">
				<!-- BEGIN col-3 -->
				<div class="col-xl-4 col-md-6">
					<div class="widget widget-stats bg-blue">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Saldo {{$data->tipe}}</h4>
							<p>Rp.{{uang(saldo_simpanan($tipe_id,0))}}</p>	
						</div>
						
					</div>
				</div>
				<div class="col-xl-4 col-md-6">
					<div class="widget widget-stats bg-green">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Saldo {{$data->tipe}} Tahun {{$tahun}}</h4>
							<p>Rp.{{uang(saldo_simpanan($tipe_id,$tahun))}}</p>	
						</div>
						
					</div>
				</div>
				<!-- END col-3 -->
				<!-- BEGIN col-3 -->
				<div class="col-xl-4 col-md-6">
					<div class="widget widget-stats bg-info">
						<div class="stats-icon"><i class="fa fa-users"></i></div>
						<div class="stats-info">
							<h4>Anggota Aktif</h4>
							<p>{{jumlah_anggota(0)}}</p>
						</div>
						
					</div>
				</div>
				
			</div>
			<div class="row">
				
				<div class="col-xl-12">
					<!-- BEGIN panel -->
					<div class="panel panel-inverse">
						<!-- BEGIN panel-heading -->
						<div class="panel-heading">
							<h4 class="panel-title">&nbsp;</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-default" data-toggle="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-success" data-toggle="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-warning" data-toggle="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-danger" data-toggle="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						
						<div class="panel-body">
                            <div class="row">
                                <div class="col-md-8 ">
                                   
                                    <span onclick="tambah_data(`{{encoder(0)}}`)" class="btn btn-sm btn-primary mb-3"><i class="fas fa-plus"></i> Add Simpanan</span>
                                    
                                </div>
                                <div class="col-md-4 ">
                                    <input type="text" id="cari_data" placeholder="Cari............" class="form-control">
                                </div>
                            </div>
                            <form  id="mydata" method="post" action="{{ url('simpanan') }}?tgl_simpanan={{date('Y-m-d')}}" enctype="multipart/form-data" >
                                <!-- <input type="submit"> -->
                                <input type="hidden" name="tipe_id" value="{{$tipe_id}}">             
                                <input type="hidden" name="status_keuangan_id" value="1">             
                                @csrf
                                <div class="table-responsive">
                                    <table width="100%" id="data-table-fixed-header" class="table table-bordered align-middle">
                                        <thead>
                                            <tr role="row">
                                                <th width="1%">No</th>
                                                <th width="4%"></th>
                                                <th width="10%">Action</th>
                                                <th width="10%">No.Anggota</th>
                                                <th >Nama</th>
                                                <th width="15%">Saldo</th>
                                                <th width="7%">Periode</th>
                                                <th width="15%">Update</th>
                                            
                                            </tr>
                                        </thead>

                                    
                                    </table>
                                </div>
                            </form>
						</div>
						
					</div>
					<!-- END panel -->
				</div>
				<!-- END col-10 -->
			</div>
			<!-- END row -->
		</div>

        <div id="modal-bayar" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:70%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Form Pembayaran {{$data->tipe}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="note alert-gray-500 mb-3">
                            <div class="note-content">
                                <h4><b>Perhatian !</b></h4>
                                <p>
                                    Jika Nama Anggota Yang dipilih dengan periode yang sudah terdaftar maka system akan melakukan update pada tanggal tagihannya
                                </p>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="form-label col-form-label col-md-4">Periode Bayar *</label>
                            <div class="col-md-3">
                                <select class="exselect2 form-control"  id="bulan" >
                                    <option value="">.::Pilih------</option>
                                    @for($x=1;$x<13;$x++)
                                    <option value="{{ubah_bulan($x)}}" > {{bulan(ubah_bulan($x))}}</option>
                                    @endfor
                                </select>
                                
                            </div>
                            <div class="col-md-2">
                                <select class="exselect2 form-control"  id="tahun" >
                                    <option value="">.::Pilih------</option>
                                    @for($x=2022;$x<(date('Y')+1);$x++)
                                    <option value="{{$x}}" > {{$x}}</option>
                                    @endfor
                                </select>
                                
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label class="form-label col-form-label col-md-4">Nilai Simpanan *</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control form-control-sm " placeholder="Ketik....." value="{{$data->nilai}}" id="nilai_rupiah"  />
                                
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="form-label col-form-label col-lg-4">Tanggal Pembayaran</label>
                            <div class="col-lg-3">
                                <div class="input-group input-group-sm date" id="datepicker-disabled-past"  >
                                    <input type="text" id="tgl_simpanan"  class="form-control" placeholder="yyyy-mm-dd" />
                                    <span class="input-group-text input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Tutup</a>
                        <a href="javascript:;" class="btn btn-primary" onclick="simpan_data()">Simpan</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div> 
        <div id="modal-filter" class="modal fade flip " tabindex="-1" aria-labelledby="flipModalLabel" aria-hidden="true" style="display: none;top: -30px;">                                               
            <div class="modal-dialog" style="max-width:30%">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Filter Tahun</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-2">
                            <label class="form-label  col-md-12">Pilih Tahun Pencarian</label>
                            <div class="col-md-12">
                                <select class="form-control form-control-sm "   id="tahunnya"  >
                                    <option value="">.::Pilih------</option>
                                    @for($x=2022;$x<(date('Y')+1);$x++)
                                        <option value="{{$x}}" @if($tahun==$x) selected @endif >{{$x}}</option>
                                    @endfor
                                </select>
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="javascript:;" class="btn btn-white" data-dismiss="modal">Tutup</a>
                        <a href="javascript:;" class="btn btn-blue" onclick="proses_cari()">Cari</a>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
@endsection

@push('function-ajax')
    <script>
        function tambah_data(id) {
    
            $('#modal-bayar').modal('show');
        }
        function proses_cari() {
            var tahun=$('#tahunnya').val();
            location.assign("{{ url('simpanan') }}?tipe={{encoder($tipe_id)}}&tahun="+tahun)
        }
        function filter_data() {
    
           $('#modal-filter').modal('show');
        }
        function simpan_data(){
            var bulan=$('#bulan').val();
            var tahun=$('#tahun').val();
            var nilai=$('#nilai_rupiah').val();
            var tgl_simpanan=$('#tgl_simpanan').val();
            var form=document.getElementById('mydata');
            if(bulan=="" || tahun=="" || nilai==""){
                    swal({
                        title: 'Opps Error!',
                        html:true,
                        text:'ss',
                        icon: 'error',
                        buttons: {
                            cancel: {
                                text: 'Tutup',
                                value: null,
                                visible: true,
                                className: 'btn btn-default',
                                closeModal: true,
                            },
                            
                        }
                    });
                    $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">Harap Lengkapi Pengisian</div>')
            }else{
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('simpanan') }}?nilai="+nilai+"&bulan="+bulan+"&tahun="+tahun+"&tgl_simpanan="+tgl_simpanan,
                        data: new FormData(form),
                        contentType: false,
                        cache: false,
                        processData:false,
                        beforeSend: function() {
                            document.getElementById("loadnya").style.width = "100%";
                        },
                        success: function(msg){
                            var bat=msg.split('@');
                            if(bat[1]=='ok'){
                                document.getElementById("loadnya").style.width = "0px";
                                
                                location.reload();
                            }else{
                                document.getElementById("loadnya").style.width = "0px";
                                
                                swal({
                                    title: 'Opps Error!',
                                    html:true,
                                    text:'ss',
                                    icon: 'error',
                                    buttons: {
                                        cancel: {
                                            text: 'Tutup',
                                            value: null,
                                            visible: true,
                                            className: 'btn btn-default',
                                            closeModal: true,
                                        },
                                        
                                    }
                                });
                                $('.swal-text').html('<div style="width:100%;background:#f2f2f5;padding:2%;text-align:left;font-size:13px">'+msg+'</div>')
                            }
                            
                            
                        }
                    });
            }
        }
        function delete_data(id){
                swal({
                        title: "Yakin menghapus data  ini ?",
                        text: "data akan hilang dari daftar  ini",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'GET',
                            url: "{{url('anggota/delete_data')}}",
                            data: "id="+id,
                            success: function(msg){
                                swal("Sukses diproses", "", "success")
                                var table=$('#data-table-fixed-header').DataTable();
                                    table.ajax.url("{{ url('simpanan/get_data')}}").load();
                              
                            }
                        });
                        
                    } else {
                        var table=$('#data-table-fixed-header').DataTable();
                            table.ajax.url("{{ url('simpanan/get_data')}}").load();
                    }
                });
                
            
        } 
    </script>
@endpush